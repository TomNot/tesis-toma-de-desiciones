#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 21:41:31 2021

@author: Tomas
"""

from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm, tnrange
import seaborn as sns
import theano
import arviz as az
import copy 
from scipy.integrate import quad

#%%
#x es el rango de las betas
# x=np.arange(start=1/50,stop=49/50,step=1/50) 
    

## Bayesian inference over parameters Thompson sampling
    
# Defino una función que plotea los resultados de la simulación

def plot_data(actions, rewards, Qs):
    plt.figure(figsize=(20,3))
    x = np.arange(len(actions))

    plt.plot(x, Qs[:,0] - .5 + 0, c='C0', lw=3, alpha=.3)
    plt.plot(x, Qs[:,1] - .5 + 1, c='C1', lw=3, alpha=.3)

    s = 50
    lw = 2

    cond = (actions == 0) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C0', lw=lw)

    cond = (actions == 0) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C0', ec='C0', lw=lw)

    cond = (actions == 1) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C1', lw=lw)

    cond = (actions == 1) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C1', ec='C1', lw=lw)

    plt.scatter(0, 20, c='k', s=s, lw=lw, label='Reward')
    plt.scatter(0, 20, c='w', ec='k', s=s, lw=lw, label='No reward')
    plt.plot([0,1], [20, 20], c='k', lw=3, alpha=.3, label='b_value (centered)')


    plt.yticks([0,1], ['left', 'right'])
    plt.ylim(-1, 2)

    plt.ylabel('action')
    plt.xlabel('trial')

    handles, labels = plt.gca().get_legend_handles_labels()
    order = (1,2,0)
    handles = [handles[idx] for idx in order]
    labels = [labels[idx] for idx in order]

    plt.legend(handles, labels, fontsize=12, loc=(1.01, .27))
    plt.tight_layout()

def integrandB(x, eA, fA, eB, fB):
    return stats.beta.pdf(x, eA+1, fA+1)*(1-stats.beta.cdf(x, eB+1, fB+1))

def confidence(parA, parB):
    # print(parA)
    # print(parB)
    
    IA = quad(integrandB, 0, 1, args=(*parB, *parA))[0]
    IB = quad(integrandB, 0, 1, args=(*parA, *parB))[0]
    
    # print(IA, IB)
    
    N = np.sum([IA, IB])
    
    res = [IA/N, IB/N]
    
    return np.array(res)


# Genero data bayesiana
    
def generate_data_bayes(priorA, priorB, n=100, p_r=[.4, .6]):
    # paso de b,w a e,f
    expA = np.array([priorA[0]*priorA[1], priorA[1]-priorA[0]*priorA[1]]) #[e, f]
    expB = np.array([priorB[0]*priorB[1], priorB[1]-priorB[0]*priorB[1]]) #[e, f]
    
    #experiencia verdadera (sin prior)
    expAr = np.array([0, 0]) #[e, f]
    expBr = np.array([0, 0]) #[e, f]
    
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    
    Ps = np.zeros((n, 2))
    
    for i in range(n):
        # print(i)
        Ps[i] = np.array([expA[0]/np.sum(expA), expB[0]/np.sum(expB)])
        # Simulate choice and reward
        
        #eleccion de maquina 0 o 1
        if stats.beta.rvs(*expA,1)>stats.beta.rvs(*expB,1):
            eleccion=0
        else:
            eleccion=1    
        resultado = np.random.rand() < p_r[eleccion]
        
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            expA[0]=expA[0]+1
            expAr[0]=expAr[0]+1
        elif eleccion==0 and resultado==0:
            expA[1]=expA[1]+1
            expAr[1]=expAr[1]+1
        elif eleccion==1 and resultado==1:
            expB[0]=expB[0]+1
            expBr[0]=expBr[0]+1
        elif eleccion==1 and resultado==0:
            expB[1]=expB[1]+1
            expBr[1]=expBr[1]+1


        # Store values
        actions[i] = eleccion
        rewards[i] = resultado

    return actions, rewards, Ps, expA, expB, expAr, expBr

# Genero data RL asimétrico

def generate_data_RLas(alpha_s, alpha_f, beta, n=100, p_r=[.4, .6]):
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    Qs = np.zeros((n, 2))

    # Initialize Q table
    Q = np.array([.5, .5])
    for i in range(n):
        # Apply the Softmax transformation
        exp_Q = np.exp(beta*Q)
        prob_a = exp_Q / np.sum(exp_Q)

        # Simulate choice and reward
        a = np.random.choice([0, 1], p=prob_a)
        r = np.random.rand() < p_r[a]

        # Update Q table
        if r:
            Q[a] = Q[a] + alpha_s * (r - Q[a])
            # print('r=1')
        else:
            Q[a] = Q[a] + alpha_f * (r - Q[a])
            # print('r=0')

        # Store values
        actions[i] = a
        rewards[i] = r
        Qs[i] = copy.deepcopy(Q) # copio para no sobreescribir la variable

    return actions, rewards, Qs


#%% defino las funciones para el modelo RW asimétrico

def update_Q(action, reward,
             Qs,
             alphas):
    """
    This function updates the Q table according to the RL update rule.
    It will be called by tt.scan to do so recursevely, given the observed data and the alpha parameter
    This could have been nlaced be the following lamba expression in the tt.scan fn argument:
        fn=lamba action, reward, Qs, alpha: tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    """
    # print('\n')
    # print(tt.shape(alphas).eval())
    Qs = tt.set_subtensor(Qs[action], Qs[action] + alphas[reward] * (reward - Qs[action]))
    return Qs

# Transform the variables into appropriate Theano objects
def theano_llik_td(alphas, beta, actions, rewards):
    rewards = theano.shared(np.asarray(rewards, dtype='int16'))
    actions = theano.shared(np.asarray(actions, dtype='int16'))

    # Compute the Qs values
    Qs = 0.5 * tt.ones((2), dtype='float64')
    Qs, updates = theano.scan(
        fn=update_Q,
        sequences=[actions, rewards],
        outputs_info=[Qs],
        non_sequences=[alphas])#[var]

    # Apply the sotfmax transformation
    Qs_ = Qs[:-1] * beta
    log_prob_actions = Qs_ - pm.math.logsumexp(Qs_, axis=1)

    # Calculate the negative log likelihod of the observed actions
    log_prob_actions = log_prob_actions[tt.arange(actions.shape[0]-1), actions[1:]]
    return tt.sum(log_prob_actions)  # PyMC makes it negative by default

def softmax(beta, Q, action):
    return np.exp(beta * Q[action])/np.sum(np.exp(beta*Q))
# #%% Ajusto la grilla de b y comparo con los valores reales
n=100
trials = np.arange(1,n+1)
# p_r = [.2, .8]
agentes = 50
bs = np.linspace(0.05, 0.95, agentes)
repeticiones = 1
# prior = [.6, 8]
w_fit = 50
npar = 1

datatot = np.zeros((npar,2,repeticiones,agentes))
res = np.zeros((n,2,repeticiones,agentes))

# for j, b in tqdm(enumerate(bs), desc='Loop principal 0'):
#     dataprom = np.zeros((npar,2, repeticiones))
#     resultados = np.zeros((n,2, repeticiones))
    
#     prior = [b, w_fit]
    
#     for k in tqdm(range(repeticiones), desc='Loop promedio 1'):   
        
#         actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(prior, prior, n=n, p_r=p_r)
            
#           #inicializo las listas
#         data = np.array((actions, rewards)).T
        
#         resultados[:,:,k] = data
    
#         p_prob = np.zeros((len(bs), n))
#         prob = np.zeros((len(bs), n))
        
#         #experiencia verdadera (sin prior)
#         s0, f0, s1, f1 = np.zeros((4))
        
#         for l in tqdm(range(n), desc='Loop trials 2'):
#             # print(l)
#             eleccion = actions[l]
#             resultado = rewards[l]
#             #actializo parametros de Beta posterior (usada para comportamiento)
#             #y actualizo la experiencia real (usada luego para el fit)
            
#             if eleccion==0 and resultado==1:
#                 s0=s0+1
#             elif eleccion==0 and resultado==0:
#                 f0=f0+1
#             elif eleccion==1 and resultado==1:
#                 s1=s1+1
#             elif eleccion==1 and resultado==0:
#                 f1=f1+1
                   
#             #llamo b_fit al unico parametros libre (asumo b0=b1 y w verdadero)
#             for i_0, b_fit in tqdm(enumerate(bs), desc='Loop likelihood 3'):
                
#                 # print('b='+str(b_fit)+'\nw='+str(w_fit))
                  
#                 s0_p = b_fit*w_fit
#                 f0_p = (w_fit-b_fit*w_fit)
                
#                 s1_p = b_fit*w_fit
#                 f1_p = (w_fit-b_fit*w_fit)
                
#                 # print(s0_p+s0_repeticiones,f0_p+f0)
#                 # print(s1_p+s1,f1_p+f1)
#                 # print(l, b_fit)
                
#                 s0_real = s0_p + s0
#                 f0_real = f0_p + f0
#                 s1_real = s1_p + s1
#                 f1_real = f1_p + f1
                
#                 #computo pdf's y cdf's
#                 prob0, prob1 = confidence([s0_real, f0_real], [s1_real,f1_real])
                
#                 # print('prob0='+str(prob0))
#                 # print('prob1='+str(prob1))
                
#                 #probabilidad de la eleccion dado el prior (labeled by i_0)
#                 if eleccion==0:
#                     p_prob[i_0,l]=np.log(prob0)
#                 elif eleccion==1:
#                     p_prob[i_0,l]=np.log(prob1)
               
#                 #i.i.d. + np.log sapce
#                 prob[i_0,l]=np.sum(p_prob[i_0,1:l])
                   
#             #normalizo y paso a linear space
                        
#             prob[:,l]=np.exp(prob[:,l])/np.sum(np.exp(prob[:,l]))
            
#         dataprom[0,:,k] = [np.average(bs, weights=prob[:,-1]), np.sqrt(np.cov(bs, aweights=prob[:,-1]))]
#         # dataprom[1,:,k] = [np.average(ws, weights=prob[:,-1]), np.sqrt(np.cov(ws, aweights=prob[:,-1]))]
    
#     res[:,:,:,j] = resultados
#     datatot[:,:,:,j] = dataprom
# np.savez('correlacion_ajuste_bayes_N%i_%iagentes_w%i(%i rep).npz'%(n, agentes,w_fit,repeticiones), bs, datatot, res)
#%% Ajusto y grafico

# arrays = np.load('correlacion_ajuste_bayes_N100_50agentes_w50(1 rep).npz')

# bs, datatot, res = [arrays['arr_%i'%i] for i in range(3)]

# x = np.linspace(0,1,10000)
# bfit = datatot.mean(axis=2)[0,0,:]
# berr = datatot.mean(axis=2)[0,1,:]/np.sqrt(repeticiones)
# model1 = np.polyfit(bs, bfit, 1, w=1/berr)
# plt.plot(bs, model1[0]*bs+model1[1], 'k-', alpha=0.4, ls='dashed')
# plt.errorbar(bs, bfit, yerr=berr, fmt='r.', uplims=True, lolims=True)
# plt.plot(x, x, ls='solid', alpha=0.9, color='k')
# plt.xlabel(r'$b_{true}$')
# plt.ylabel(r'$b_{fit}$')
# plt.xlim(0,1)
# plt.ylim(0,1)
# plt.grid()
# plt.savefig('correlacion_ajuste_%iagentes.png'%agentes, dpi=400)

# corr, p = pearsonr(bs, bfit)
#%% Ajusto la grilla de dif_alpha y comparo con los valores reales
n=100
trials = np.arange(1,n+1)
p_r = [.6, .8]

agentes = 50

# alpha_prom = 0.4

# n = 100

repeticiones = 10

# defino los parámetros de los agentes (recorro toda la grilla)
alpha_prom = 0.6
dif_alphas = np.linspace(-alpha_prom/2, alpha_prom/2, agentes)
alpha_mas = alpha_prom + dif_alphas/2
alpha_menos = alpha_prom - dif_alphas/2
true_beta = 6

# repeticiones = 5
# prior = [.6, 8]

npar = 3

datatot = np.zeros((npar,2,repeticiones,agentes))
res = np.zeros((n,2,repeticiones,agentes))

for i, [true_alpha_s, true_alpha_f] in tqdm(enumerate(zip(alpha_mas, alpha_menos)), desc='Loop agentes'): 
      
    dataprom = np.zeros((npar,2,repeticiones))
    
    resultados = np.zeros((n,2,repeticiones))
    
    for k in tqdm(range(repeticiones), desc='Loop promedio'):   
        
        actions, rewards, Qs = generate_data_RLas(true_alpha_s, true_alpha_f, true_beta, n, p_r=p_r)
            
        resultados[:,:,k] = np.array([actions, rewards]).T
                   
        with pm.Model() as m:
            # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
            # beta = pm.Exponential('beta', 10, testval=5)
            
            # funciona mejor con estos priors que con los anteriores
            
            alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([true_alpha_f, true_alpha_s])) #empiezo de una uniforme
            beta = pm.Normal('beta', mu=5, sigma=.01, testval=5)
            
            
            # pm.Potential me permite definir "distribuciones" arbitrarias
            
            like = pm.Potential('like', theano_llik_td(alphas, beta, actions, rewards))
        
            # trace = pm.sample() # sampleo por defecto
            # burned_trace=trace[1000:]
            
            # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                                # era para arreglar un error
            
            step = pm.Metropolis()
            trace = pm.sample(20000, step=step)
            burned_trace=trace[1000:]
            
        par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
        print(par_fit)
        az.plot_posterior(burned_trace);
        
        plt.savefig('posterior_%iagentes_RLas.png'%agentes, dpi=400)
        
                
        datatot[:,:,k,i] = par_fit
        
        res[:,:,k,i] = resultados[:,:,k]

np.savez('correlacion_ajuste_RLas_N%i_%iagentes_alpha_mean%i(%i rep)pA%.2fpB%.2f.npz'%(n, agentes,alpha_prom,repeticiones, *p_r), dif_alphas, datatot, res)
#%% Ajusto y grafico
from scipy.stats import pearsonr
arrays = np.load('correlacion_ajuste_RLas_N100_50agentes_alpha_mean0(10 rep)pA0.20pB0.80.npz')

dif_alphas, datatot, res = [arrays['arr_%i'%i] for i in range(3)]

repeticiones = 10

x = np.linspace(min(dif_alphas),max(dif_alphas),10000)
parfit = datatot.mean(axis=2)[:,0,:]
errpar = datatot.mean(axis=2)[:,1,:]/np.sqrt(repeticiones)

dif_alphas_fit = parfit[2,:] - parfit[1,:]
err_dif_alphas_fit = np.sqrt(errpar[1]**2 + errpar[2]**2)

model1 = np.polyfit(dif_alphas, dif_alphas_fit, 1)#, w=1/err_dif_alphas_fit)
plt.figure()
plt.plot(dif_alphas, model1[0]*dif_alphas+model1[1], 'k-', alpha=0.4, ls='dashed')
plt.errorbar(dif_alphas, dif_alphas_fit, yerr=err_dif_alphas_fit, fmt='r.', uplims=True, lolims=True)
plt.plot(x, x, ls='solid', alpha=0.9, color='k')
plt.xlabel(r'$\Delta \alpha_{true}$')
plt.ylabel(r'$\Delta \alpha_{fit}$')
# plt.xlim(0,1)
# plt.ylim(0,1)
plt.grid()
plt.savefig('correlacion_ajuste_%iagentes_RLas.png'%agentes, dpi=400)

corr, p = pearsonr(dif_alphas, dif_alphas_fit)
#%%
x_mas = np.linspace(min(alpha_mas),max(alpha_mas),10000)

alphas_f_fit = parfit[1]
err_alphas_f_fit = errpar[1]
alphas_s_fit = parfit[2]
err_alphas_s_fit = errpar[2]

model1 = np.polyfit(alpha_mas, alphas_s_fit, 1, w=1/err_alphas_s_fit)
plt.figure()
plt.plot(alpha_mas, model1[0]*alpha_mas+model1[1], 'k-', alpha=0.4, ls='dashed')
plt.errorbar(alpha_mas, alphas_s_fit, yerr=err_alphas_s_fit, fmt='r.', uplims=True, lolims=True)
plt.plot(x_mas, x_mas, ls='solid', alpha=0.9, color='k')
plt.xlabel(r'$\alpha_+^{true}$')
plt.ylabel(r'$\alpha_+^{fit}$')
# plt.xlim(0,1)
# plt.ylim(0,1)
plt.grid()
plt.savefig('correlacion_ajuste_%iagentes_RLas_mas.png'%agentes, dpi=400)

#%%
x_menos = np.linspace(min(alpha_menos),max(alpha_menos),10000)
model1 = np.polyfit(alpha_menos, alphas_f_fit, 1, w=1/err_alphas_f_fit)
plt.figure()
plt.plot(alpha_menos, model1[0]*alpha_menos+model1[1], 'k-', alpha=0.4, ls='dashed')
plt.errorbar(alpha_menos, alphas_f_fit, yerr=err_alphas_f_fit, fmt='r.', uplims=True, lolims=True)
plt.plot(x_menos, x_menos, ls='solid', alpha=0.9, color='k')
plt.xlabel(r'$\alpha_-^{true}$')
plt.ylabel(r'$\alpha_-^{fit}$')
# plt.xlim(0,1)
# plt.ylim(0,1)
plt.grid()
plt.savefig('correlacion_ajuste_%iagentes_RLas_menos.png'%agentes, dpi=400)

corr,p = pearsonr(alpha_menos, alphas_f_fit)
