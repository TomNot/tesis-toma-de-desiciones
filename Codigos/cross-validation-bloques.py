#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 25 10:42:39 2021

@author: Tomas
"""


# Importo las librerías para graficar
from IPython.core.pylabtools import figsize
import matplotlib.pyplot as plt
import numpy as np

# Cargo los datos de las simulaciones y grafico los BICs
# Genero con bayes + Thompson sampling y ajusto con RW asimétrico

arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-decisiones/Datos_simulados/BIC_RLas_RLas_N20_13_esc(1 rep).npz')

dataprom = arrays['arr_0']
BICs = arrays['arr_1']
resultados = arrays['arr_2']

BICprom = BICs.mean(axis=0).mean(axis=1)
BICstd = BICs.std(axis=0, ddof=1).mean(axis=1)

arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-decisiones/Datos_simulados/BIC_bayes_bayes_N20_13_esc(1 rep).npz')

bdataprom = arrays['arr_0']
bBICs = arrays['arr_1']
bresultados = arrays['arr_2']

bBICprom = bBICs.mean(axis=0)
bBICstd = bBICs.std(axis=0, ddof=1)

#%% Simulo: genera bayes ajusta bayes


dim = 5

a = np.linspace(0.05, 0.95, dim)
# a = (A+B)/2

d = np.linspace(0.05, 0.95, dim)
# d = |A-B|
# supongamos que A > B entonces d = A-B

a,d = np.meshgrid(a,d)

A = a + d/2

B = a - d/2

print(A)
print(B)

#%%
Abool = np.zeros((dim,dim))
for i, row in enumerate(A):
    for j, column in enumerate(row):
        Abool[i,j] = 0<column<1
print(Abool)

Bbool = np.zeros((dim,dim))
for i, row in enumerate(B):
    for j, column in enumerate(row):
        Bbool[i,j] = 0<column<1
print(Bbool)                

aux = np.abs(Abool - Bbool)
aux = np.abs(aux - 1)

Aok = np.zeros((dim,dim))
Bok = np.zeros((dim,dim))

for i, row in enumerate(aux):
    for j, column in enumerate(row):
        Aok[i,j]=aux[i,j]*A[i,j]
        Bok[i,j]=aux[i,j]*B[i,j]

print(Aok)
print(Bok)
#%%
bloques = []
for i, row in enumerate(aux):
    for j, column in enumerate(row):
        if Aok[i,j]!=0:
            bloques.append([Aok[i,j], Bok[i,j]])
print(bloques)

bloques = np.array(bloques)
A = bloques[:,0]
B = bloques[:,1]

a = (A+B)/2
d = A-B
dif = 1 - d
#%%
# Escenarios de las simuaciones
p_r_list=bloques
np_r = len(p_r_list)

n = 20 # número de trials

total = 1 # repeticiones por escenario para promediar

figsize(10, 8)

#BICs en función del escenario

plt.figure()

ax = plt.subplot(111)

dom = np.arange(0,len(BICprom))

plt.plot(dom, BICprom,'r.', alpha=0.85, label=r'RLas')
plt.errorbar(dom, BICprom, BICstd, fmt='none', ecolor='red')
plt.plot(dom, bBICprom,'b.', alpha=0.85, label=r'bayes')
plt.errorbar(dom, bBICprom, bBICstd, fmt='none', ecolor='blue')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.xticks(ticks=dom, labels=p_r_list)
plt.legend(loc="upper right")
plt.xlabel(r'Stage $[p_A, p_B]$')
plt.ylabel(r'BIC')
plt.title(r"BIC vs Stage. $N=%i$ (%i rep)"%(n, total))
plt.grid()
#plt.savefig('BIC_vs_Stage_bayes_RLas_N%i_%i_esc(%i rep)'%(n, np_r, total), dpi=400)
#%%
alpha_s = dataprom[1, 0, 0, :, :]
err_alpha_s = dataprom[1, 1, 0, :, :]
alpha_f = dataprom[2, 0, 0, :, :]
err_alpha_f = dataprom[2, 1, 0, :, :]

dif_alphas = alpha_s - alpha_f
err_dif_alphas = np.sqrt(err_alpha_s**2 + err_alpha_f**2)
#%%
agente = 0

plt.figure()
plt.plot(dom, alpha_s[:,0],'r.', alpha=0.85, label=r'$\alpha_+$')
plt.errorbar(dom, alpha_s[:,0], err_alpha_s[:,0], fmt='none', ecolor='red')
plt.plot(dom, alpha_f[:,0],'b.', alpha=0.85, label=r'$\alpha_-$')
plt.errorbar(dom, alpha_f[:,0], err_alpha_f[:,0], fmt='none', ecolor='blue')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.xticks(ticks=dom, labels=p_r_list)
plt.legend(loc="upper right")
plt.xlabel(r'Stage $[p_A, p_B]$')
plt.ylabel(r'$\alpha$\'s')
plt.title(r'$\alpha$\'s vs Stage. $N=%i$ (%i rep)'%(n, total))
plt.grid()

plt.figure()
plt.plot(dom, dif_alphas[:,0],'r.', alpha=0.85, label=r'$\Delta \alpha$')
plt.errorbar(dom, dif_alphas[:,0], err_dif_alphas[:,0], fmt='none', ecolor='red')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.xticks(ticks=dom, labels=p_r_list)
plt.legend(loc="upper right")
plt.xlabel(r'Stage $[p_A, p_B]$')
plt.ylabel(r'$\Delta \alpha$\'s')
plt.title(r'$\Delta \alpha$\'s vs Stage. $N=%i$ (%i rep)'%(n, total))
plt.grid()
#%% clasificamos los bloques

fya, dya, fyg, dyg = [[],[],[],[]]

parameters = [[],[],[],[]]

for i, bloque in enumerate(bloques):
    if a[i] <= 0.5:
        if dif[i] <= 0.5:
            fya.append(bloque)
            parameters[0].append(np.array([dif_alphas[i,:], err_dif_alphas[i,:]]))
        else:
            fyg.append(bloque)
            parameters[1].append(np.array([dif_alphas[i,:], err_dif_alphas[i,:]]))
    else:
        if dif[i] <= 0.5:
            dya.append(bloque)
            parameters[2].append(np.array([dif_alphas[i,:], err_dif_alphas[i,:]]))
        else:
            dyg.append(bloque)
            parameters[3].append(np.array([dif_alphas[i,:], err_dif_alphas[i,:]]))
            
#%% separo un tipo y lo grafico en función de los demás
# tomo dya porque tiene muchos datos

otros = parameters[0] + parameters[2] + parameters[3]

otros = np.array(otros).mean(axis=0)
otros[1,:] = otros[1,:]/np.sqrt(dif_alphas.shape[1])

x = np.array(parameters[1]).mean(axis=0)
x[1,:] = x[1,:]/np.sqrt(dif_alphas.shape[1])


#%%
plt.figure()
plt.plot(x[0,:], otros[0,:],'r.', alpha=0.85)
plt.errorbar(x[0,:], otros[0,:], yerr=otros[1,:], xerr=x[1,:], fmt='none', ecolor='red')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
# plt.xticks(ticks=dom, labels=p_r_list)
# plt.legend(loc="upper right")
plt.xlabel(r'$\Delta \alpha$ otros')
plt.ylabel(r'$\Delta \alpha$ dif. y avaro')
plt.title(r'Cross-validation. $N=%i$ (%i rep)'%(n, total))
plt.grid()
            