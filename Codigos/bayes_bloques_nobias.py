#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 13:40:45 2021

@author: Tomas
"""
import numpy as np
import pandas as pd
from tqdm import tqdm
import matplotlib.pyplot as plt
#%%
arrays = np.load('dificultad_comportamiento_bayes_N100_10000agentes_50bloques_con_pers.npz')

ex, cok, cint, pnb, pok, bs, sigmas, sigmas_int, bloques = [arrays['arr_%i'%i] for i in range(9)] #, pers, pok

#%%
bloques = np.array(bloques)
A = bloques[:,0]
B = bloques[:,1]

a = (A+B)/2
d = A-B
dif = 1 - d

#%%
# import statsmodels.api as sm           ## Este proporciona funciones para la estimación de muchos modelos estadísticos
import statsmodels.formula.api as smf  ## Permite ajustar modelos estadísticos utilizando fórmulas de estilo R

def sigmoidea(coefs):
    return 1/(1+np.exp(-coefs[0]/-coefs[1]))

sigmas = np.zeros((len(bs)))
sigmas_int = np.zeros((len(bs)))

cok = np.abs(cok)

for i in tqdm(range(len(bs))):
    
    dtint = pd.DataFrame(data=np.matrix([a,dif,pnb[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    dt = pd.DataFrame(data=np.matrix([a,dif,pok[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    
    mod = smf.ols('confianza ~ atractivo + dificultad', data=dt).fit()
    modint = smf.ols('confianza ~ atractivo + dificultad', data=dtint).fit()
    # print(mod.summary())
    coefs = mod.params
    coefsint = modint.params
    sigma = sigmoidea(coefs[1:])
    sigmaint = sigmoidea(coefsint[1:])
    
    print(coefs)
    print(sigma)
    print(coefsint)
    print(sigmaint)
    
    sigmas[i] = sigma
    sigmas_int[i] = sigmaint
#%%
def lineal(x,m,b):
    return m*x+b

dom = ex.mean(axis=0)
# pers = 

plt.figure()
plt.plot(dom, sigmas, '.')
# plt.plot(dom, lineal(dom, *coefs), 'r-', alpha=0.6)
plt.hlines(0.5, min(dom), max(dom), ls='dashed', label=r'$0.5$')
plt.legend(loc="upper right")
plt.xlabel(r'Alternancias promedio')
plt.ylabel(r'$\sigma$')
# plt.savefig('bayes_%ibloques_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
#%%
plt.figure()

plt.plot(dom, sigmas_int, '.')
# plt.plot(dom, lineal(dom, *coefsint), 'r-', alpha=0.6)
plt.hlines(0.5, min(dom), max(dom), ls='dashed', label=r'$0.5$')
plt.legend(loc="upper right")
plt.xlabel(r'Alternancias promedio')
plt.ylabel(r'$\sigma_{\int}$')
# plt.savefig('int_bayes_%ibloques_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)