#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan  6 15:35:19 2021

@author: Tomas
"""

from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm, tnrange
import seaborn as sns
import theano
import arviz as az
import copy 
import arviz as az
from scipy.integrate import quad

#%%
def integrandB(x, eA, fA, eB, fB):
    return stats.beta.pdf(x, eA+1, fA+1)*(1-stats.beta.cdf(x, eB, fB))

def confidence(parA, parB):
    IA = quad(integrandB, 0, 1, args=(*parB, *parA))
    IB = quad(integrandB, 0, 1, args=(*parA, *parB))
    
    N = np.sum([IA, IB])
    
    res = [IA/N, IB/N]
    
    return np.array(res)
    
    #x es el rango de las betas
# x=np.arange(start=1/50,stop=49/50,step=1/50) 
    

## Bayesian inference over parameters Thompson sampling
    
# Defino una función que plotea los resultados de la simulación

def plot_data(actions, rewards, Qs):
    plt.figure(figsize=(20,3))
    x = np.arange(len(actions))

    plt.plot(x, Qs[:,0] - .5 + 0, c='C0', lw=3, alpha=.3)
    plt.plot(x, Qs[:,1] - .5 + 1, c='C1', lw=3, alpha=.3)

    s = 50
    lw = 2

    cond = (actions == 0) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C0', lw=lw)

    cond = (actions == 0) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C0', ec='C0', lw=lw)

    cond = (actions == 1) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C1', lw=lw)

    cond = (actions == 1) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C1', ec='C1', lw=lw)

    plt.scatter(0, 20, c='k', s=s, lw=lw, label='Reward')
    plt.scatter(0, 20, c='w', ec='k', s=s, lw=lw, label='No reward')
    plt.plot([0,1], [20, 20], c='k', lw=3, alpha=.3, label='b_value (centered)')


    plt.yticks([0,1], ['left', 'right'])
    plt.ylim(-1, 2)

    plt.ylabel('action')
    plt.xlabel('trial')

    handles, labels = plt.gca().get_legend_handles_labels()
    order = (1,2,0)
    handles = [handles[idx] for idx in order]
    labels = [labels[idx] for idx in order]

    plt.legend(handles, labels, fontsize=12, loc=(1.01, .27))
    plt.tight_layout()
# Genero data 
    
def generate_data(priorA, priorB, n=100, p_r=[.4, .6]):
    # paso de b,w a e,f
    expA = np.array([priorA[0]*priorA[1], priorA[1]-priorA[0]*priorA[1]]) #[e, f]
    expB = np.array([priorB[0]*priorB[1], priorB[1]-priorB[0]*priorB[1]]) #[e, f]
    
    #experiencia verdadera (sin prior)
    expAr = np.array([0, 0]) #[e, f]
    expBr = np.array([0, 0]) #[e, f]
    
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    
    Ps = np.zeros((n, 2))
    
    for i in range(n):
        # print(i)
        Ps[i] = np.array([expA[0]/np.sum(expA), expB[0]/np.sum(expB)])
        # Simulate choice and reward
        
        #eleccion de maquina 0 o 1
        if stats.beta.rvs(*expA,1)>stats.beta.rvs(*expB,1):
            eleccion=0
        else:
            eleccion=1    
        resultado = np.random.rand() < p_r[eleccion]
        
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            expA[0]=expA[0]+1
            expAr[0]=expAr[0]+1
        elif eleccion==0 and resultado==0:
            expA[1]=expA[1]+1
            expAr[1]=expAr[1]+1
        elif eleccion==1 and resultado==1:
            expB[0]=expB[0]+1
            expBr[0]=expBr[0]+1
        elif eleccion==1 and resultado==0:
            expB[1]=expB[1]+1
            expBr[1]=expBr[1]+1


        # Store values
        actions[i] = eleccion
        rewards[i] = resultado

    return actions, rewards, Ps, expA, expB, expAr, expBr


#largo del bloque bandit
n = 100

#probabilidades de pago verdaderas
p_r=[.4, .5]

#prior del agente thompson sampling
prior = [0.5, 1] #[b, w]
priorA = prior #[b, w]
priorB = prior #[b, w]


actions, rewards, Ps, expA, expB, expAr, expBr = generate_data(priorA, priorB, n=n, p_r=p_r)

bs = np.array([expA[0]/np.sum(expA), expB[0]/np.sum(expB)])

plot_data(actions, rewards, Ps)
#%%
def update_Q(action, reward,
             Qs,
             alphas):
    """
    This function updates the Q table according to the RL update rule.
    It will be called by tt.scan to do so recursevely, given the observed data and the alpha parameter
    This could have been ntrialslaced be the following lamba expression in the tt.scan fn argument:
        fn=lamba action, reward, Qs, alpha: tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    """
    Qs = tt.set_subtensor(Qs[action], Qs[action] + alphas[reward] * (reward - Qs[action]))
    return Qs

#%%
# Transform the variables into appropriate Theano objects
def theano_llik_td(alphas, beta, actions, rewards):
    rewards = theano.shared(np.asarray(rewards, dtype='int16'))
    actions = theano.shared(np.asarray(actions, dtype='int16'))

    # Compute the Qs values
    Qs = 0.5 * tt.ones((2), dtype='float64')
    Qs, updates = theano.scan(
        fn=update_Q,
        sequences=[actions, rewards],
        outputs_info=[Qs],
        non_sequences=[alphas])#[var]

    # Apply the sotfmax transformation
    Qs_ = Qs[:-1] * beta
    log_prob_actions = Qs_ - pm.math.logsumexp(Qs_, axis=1)

    # Calculate the negative log likelihod of the observed actions
    log_prob_actions = log_prob_actions[tt.arange(actions.shape[0]-1), actions[1:]]
    return tt.sum(log_prob_actions)  # PyMC makes it negative by default

#%%
n = 10
npar = 3

trials = np.arange(1,n+1)

ntrials = len(trials)

total = 1

dataprom = np.zeros((npar,2,ntrials,total))

resultados = np.zeros((ntrials,2,total))

for k in tqdm(range(total)):
    data = np.zeros((npar,2,ntrials))
    i=-1
    
    actions, rewards, Ps, expA, expB, expAr, expBr = generate_data(priorA, priorB, n=n, p_r=p_r)
    
    resultados[:,:,k] = np.array([actions, rewards]).T
    
    for h_trial in tqdm(trials):
        i+=1
        
        with pm.Model() as m:
            # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
            # beta = pm.Exponential('beta', 10, testval=5)
            
            # funciona mejor con estos priors que con los anteriores
            
            alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([.1, .3])) #empiezo de una uniforme
            beta = pm.Normal('beta', mu=5, sigma=.1, testval=5)
            
            
            # pm.Potential me permite definir "distribuciones" arbitrarias
            
            like = pm.Potential('like', theano_llik_td(alphas, beta, actions[:h_trial], rewards[:h_trial]))
        
            # trace = pm.sample() # sampleo por defecto
            # burned_trace=trace[1000:]
            
            # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                                # era para arreglar un error
            
            step = pm.Metropolis()
            trace = pm.sample(20000, step=step)
            burned_trace=trace[1000:]
            
        par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
        print(par_fit)
        data[:,:,i] = par_fit
        print(data[:,:,i])
    dataprom[:,:,:,k] = data
    
data = dataprom.mean(axis=3)
np.savez('bayes_RLas_N%i_w1(%i rep).npz'%(n, total), data)
#%%
n = 100

total = 10

data = np.load('bayes_RLas_N%i_w1(%i rep).npz'%(n, total))['arr_0']

figsize(10, 8)

#histogram of alphas

plt.figure()

ax = plt.subplot(111)

plt.xlim(0, n+1)
plt.plot(trials, data[1,0,:]/(data[1,0,:] + data[2,0,:]),'r.', alpha=0.85, label=r'$\frac{\alpha_-}{\alpha_+ + \alpha_-}$')
plt.plot(trials, data[2,0,:]/(data[1,0,:] + data[2,0,:]),'b.', alpha=0.85, label=r'$\frac{\alpha_+}{\alpha_+ + \alpha_-}$')
plt.hlines(0.5, trials[0], n+1, ls='dashed', label='0.5')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.legend(loc="upper right")
plt.xlabel(r'$N$')
plt.ylabel(r'$X_\alpha$ fit')
plt.title(r"$X_\alpha$'s fiteados vs trials. $N=%i$ (%i rep)"%(n, total))
plt.savefig('bayes_RLas_N%i_w1(%i rep).png'%(n, total), dpi=400)
#%% Cuenta simple que hicimos con Pablo relacionando RWas con bayesiano 
# No tiene en cuenta el sampleo de Thompon
df = pd.DataFrame(list(zip(actions, rewards)), columns=['Actions', 'Rewards'], index=['%i'%(i+1) for i in range(ntrials)])

dfA = df[df['Actions']==0]
dfB = df[df['Actions']==1]

wA = dfA['Actions'].count()
wB = dfB['Actions'].count()

eA = dfA['Rewards'].sum()
eB = dfB['Rewards'].sum()

fA = wA - eA
fB = wB - eB

alpha_masA = ( (eA+1)/(ntrials + 1) - eA/ntrials ) / (1 - eA/ntrials)
alpha_masB = ( (eB+1)/(ntrials + 1) - eB/ntrials ) / (1 - eB/ntrials)

alpha_menosA = ( (eA)/(ntrials + 1) - eA/ntrials ) / (0 - eA/ntrials)
alpha_menosB = ( (eB)/(ntrials + 1) - eB/ntrials ) / (0 - eB/ntrials)


