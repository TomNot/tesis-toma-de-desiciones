#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb dim 12:58:13 2021

@author: Tomas
"""


import numpy as np
from scipy.stats import pearsonr

dim = 10

a = np.linspace(0.05, 0.95, dim)
# a = (A+B)/2

d = np.linspace(0.05, 0.95, dim)
# d = |A-B|
# supongamos que A > B entonces d = A-B

a,d = np.meshgrid(a,d)

A = a + d/2

B = a - d/2

print(A)
print(B)

#%%
Abool = np.zeros((dim,dim))
for i, row in enumerate(A):
    for j, column in enumerate(row):
        Abool[i,j] = 0<column<1
print(Abool)

Bbool = np.zeros((dim,dim))
for i, row in enumerate(B):
    for j, column in enumerate(row):
        Bbool[i,j] = 0<column<1
print(Bbool)                

aux = np.abs(Abool - Bbool)
aux = np.abs(aux - 1)

Aok = np.zeros((dim,dim))
Bok = np.zeros((dim,dim))

for i, row in enumerate(aux):
    for j, column in enumerate(row):
        Aok[i,j]=aux[i,j]*A[i,j]
        Bok[i,j]=aux[i,j]*B[i,j]

print(Aok)
print(Bok)
#%%
bloques = []
for i, row in enumerate(aux):
    for j, column in enumerate(row):
        if Aok[i,j]!=0:
            bloques.append([Aok[i,j], Bok[i,j]])
print(len(bloques))
#%%
bloques = np.array(bloques)
A = bloques[:,0]
B = bloques[:,1]

a = (A+B)/2
d = A-B
dif = 1 - d
#%%
import matplotlib.pyplot as plt
from tqdm import tqdm
import copy 

def generate_data_RLas(alpha_s, alpha_f, beta, n=100, p_r=[.4, .6]):
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    Qs = np.zeros((n, 2))
        
    # Initialize Q table
    Q = np.array([.5, .5])
    for i in range(n):
        # Apply the Softmax transformation
        # print(Q)
        exp_Q = np.exp(beta*Q)
        # print(i)
        # print('exp_Q')
        # print(exp_Q)
        prob_a = exp_Q / np.sum(exp_Q)

        # Simulate choice and reward
        a = np.random.choice([0, 1], p=prob_a)
        r = np.random.rand() < p_r[a]

        # Update Q table
        if r:
            Q[a] = Q[a] + alpha_s * (r - Q[a])
            # print(r-0)
            # print(r-Q[a])
        else:
            Q[a] = Q[a] + alpha_f * (r - Q[a])
            # print(r-0)
        
        if Q[0]>1:
            Q[0]=1
        elif Q[1]>1:
            Q[1]=1
            
        if 0>Q[0]:
            Q[0]=0
        elif 0>Q[1]:
            Q[1]=0
        
        # Store values
        actions[i] = a
        rewards[i] = r
        Qs[i] = copy.deepcopy(Q) # copio para no sobreescribir la variable

    return actions, rewards, Qs

def softmax(beta, Q, action):
    return np.exp(beta * Q[action])/np.sum(np.exp(beta*Q))

def persistencias(actions, rewards):
    n = len(actions)
    pers = 0
    for t in range(n):
        if rewards[t]==0:
            if t<(n-1) and actions[t+1]==actions[t]:
                pers+=1
    return pers
#%% RL asimétrico
agentes = 10000

# defino los parámetros de los agentes (recorro toda la grilla)
alpha_prom = .6
dif_alphas = np.zeros((agentes))#-np.linspace(-alpha_prom/2, alpha_prom/2, agentes)
alpha_mas = alpha_prom + dif_alphas/2
alpha_menos = alpha_prom - dif_alphas/2
beta = 6

# defino la cantidad de trials
n = 100

# inicializo arrays para guardar los resultados
# resultados = np.zeros((n,2,len(bloques),len(dif_alphas)))
# Qs = np.zeros((n,2,len(bloques),len(dif_alphas)))
# confs = np.zeros((2,len(bloques),len(dif_alphas)))
cok = np.zeros((len(bloques),len(dif_alphas)))
cint = np.zeros((len(bloques),len(dif_alphas)))
ex = np.zeros((len(bloques),len(dif_alphas)))
pers = np.zeros((len(bloques),len(dif_alphas)))

for i,[a_s,a_f] in tqdm(enumerate(zip(alpha_mas, alpha_menos)), desc='Loop 0'):    
    # print(a_s,a_f)
    for b,p_r in enumerate(bloques):
        # print('\n'+'p\'s={}'.format(p_r)+'\n')
        actions, rewards, Q = generate_data_RLas(a_s, a_f, beta, n, p_r=p_r) 
        cok[b,i] = np.diff(Q[-1])
        cint[b,i] = softmax(beta,Q[-2],actions[-1])
        ex[b,i] = np.sum(np.abs(np.diff(actions)))
        pers[b,i] = persistencias(actions,rewards)
#%% RL simétrico

agentes = 6

# defino los parámetros de los agentes (recorro toda la grilla)
alpha_prom = [.6]#.6
#dif_alphas = -np.linspace(-alpha_prom/2, alpha_prom/2, agentes)
alpha_mas = alpha_prom #+ dif_alphas/2
alpha_menos = alpha_prom #- dif_alphas/2
beta = 6

# defino la cantidad de trials
n = 100

# inicializo arrays para guardar los resultados
# resultados = np.zeros((n,2,len(bloques),len(dif_alphas)))
# Qs = np.zeros((n,2,len(bloques),len(dif_alphas)))
# confs = np.zeros((2,len(bloques),len(dif_alphas)))
cok = np.zeros((len(bloques)))
cint = np.zeros((len(bloques)))
ex = np.zeros((len(bloques)))
pers = np.zeros((len(bloques)))

# for i,[a_s,a_f] in tqdm(enumerate(zip(alpha_mas, alpha_menos)), desc='Loop 0'):    
    # print(a_s,a_f)
for b,p_r in tqdm(enumerate(bloques), desc='Loop 0'):
    # print('\n'+'p\'s={}'.format(p_r)+'\n')
    actions, rewards, Q = generate_data_RLas(a_s, a_f, beta, n, p_r=p_r) 
    cok[b] = np.diff(Q[-1])
    cint[b] = softmax(beta,Q[-2],actions[-1])
    ex[b] = np.sum(np.abs(np.diff(actions)))
    pers[b] = persistencias(actions,rewards)
#%% sigmas RL simétrico 
    
cok = np.abs(cok)
import pandas as pd
# import statsmodels.api as sm           ## Este proporciona funciones para la estimación de muchos modelos estadísticos
import statsmodels.formula.api as smf  ## Permite ajustar modelos estadísticos utilizando fórmulas de estilo R

def sigmoidea(coefs):
    return 1/(1+np.exp(coefs[0]/coefs[1]))

# arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-desiciones/Datos_simulados/dificultad_comportamiento_RLas_N100_10000agentes_50bloques_amean0.8_con_pers.npz')
# # 
# # ex, cok, cint, dif_alphas, bloques = [arrays['arr_%i'%i] for i in range(5)]
# ex, cok, cint, sigmas, sigmas_int, dif_alphas, bloques = [arrays['arr_%i'%i] for i in range(7)]

# sigmas = np.zeros()
# sigmas_int = np.zeros()

for i in tqdm(range(len(dif_alphas))):
    
    dtint = pd.DataFrame(data=np.matrix([a,dif,cint]).T, columns=['atractivo','dificultad','confianza'])
    dt = pd.DataFrame(data=np.matrix([a,dif,cok]).T, columns=['atractivo','dificultad','confianza'])
    
    mod = smf.ols('confianza ~ atractivo + dificultad', data=dt).fit()
    modint = smf.ols('confianza ~ atractivo + dificultad', data=dtint).fit()
    # print(mod.summary())
    coefs = mod.params
    coefsint = modint.params
    
    sigma = sigmoidea(coefs[1:])
    sigmaint = sigmoidea(coefsint[1:])
    
    # print(coefs)
    # print('\n')
    
    # print(sigma)
    # print('\n')
    
    print(coefsint)
    print('\n')
    
    print(sigmaint)
    print('\n')
    
    # sigmas[i] = sigma
    # sigmas_int[i] = sigmaint
# np.savez('dificultad_comportamiento_RLas_N%i_%iagentes_%ibloques_amean%.1f_abs.npz'%(n, agentes, len(bloques), alpha_prom), ex, cok, cint, pers, sigmas, sigmas_int, dif_alphas, bloques)
        
#%%       
cok = np.abs(cok)
import pandas as pd
# import statsmodels.api as sm           ## Este proporciona funciones para la estimación de muchos modelos estadísticos
import statsmodels.formula.api as smf  ## Permite ajustar modelos estadísticos utilizando fórmulas de estilo R

def sigmoidea(coefs):
    return 1/(1+np.exp(coefs[0]/coefs[1]))

# arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-desiciones/Datos_simulados/dificultad_comportamiento_RLas_N100_10000agentes_50bloques_amean0.8_con_pers.npz')
# # 
# # ex, cok, cint, dif_alphas, bloques = [arrays['arr_%i'%i] for i in range(5)]
# ex, cok, cint, sigmas, sigmas_int, dif_alphas, bloques = [arrays['arr_%i'%i] for i in range(7)]

sigmas = np.zeros((len(dif_alphas)))
sigmas_int = np.zeros((len(dif_alphas)))
R2s = np.zeros((len(dif_alphas)))
R2sint = np.zeros((len(dif_alphas)))

for i in tqdm(range(len(dif_alphas))):
    
    dtint = pd.DataFrame(data=np.matrix([a,dif,cint[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    dt = pd.DataFrame(data=np.matrix([a,dif,cok[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    
    mod = smf.ols('confianza ~ atractivo + dificultad', data=dt).fit()
    modint = smf.ols('confianza ~ atractivo + dificultad', data=dtint).fit()
    # print(mod.summary())
    coefs = mod.params
    coefsint = modint.params
    
    R2s[i] = mod.rsquared
    R2sint[i] = modint.rsquared
    
    sigma = sigmoidea(coefs[1:])
    sigmaint = sigmoidea(coefsint[1:])
    
    print(mod.rsquared)
    print('\n')
    
    # print(coefs)
    # print('\n')
    
    # print(sigma)
    # print('\n')
    
    # print(coefsint)
    # print('\n')
    
    # print(sigmaint)
    # print('\n')
    
    sigmas[i] = sigma
    sigmas_int[i] = sigmaint
np.savez('dificultad_comportamiento_RLsim_N%i_%iagentes_%ibloques_amean%.1f_abs.npz'%(n, agentes, len(bloques), alpha_prom), ex, cok, cint, pers, sigmas, sigmas_int, R2s, R2sint, dif_alphas, bloques)
# %%
arrays = np.load('dificultad_comportamiento_RLas_N100_10000agentes_50bloques_amean0.6_abs.npz')#/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-desiciones/Datos_simulados/
# arrays = np.load('dificultad_comportamiento_RLsim_N100_10000agentes_50bloques_amean0.6_abs.npz')

ex, cok, cint, pers, sigmas, sigmas_int, dif_alphas, bloques = [arrays['arr_%i'%i] for i in range(8)]
cok = np.abs(cok)
cok = 0.5*cok + 0.5
#%%
dom = ex.mean(axis=0)
domnorm = dom/np.max(dom)
df = pd.DataFrame(data=np.matrix([domnorm,sigmas[::-1]]).T, columns=['alternancias','sigma'])
dfint = pd.DataFrame(data=np.matrix([domnorm,sigmas_int[::-1]]).T, columns=['alternancias','sigma'])
mod1 = smf.ols('sigma ~ alternancias', data=df).fit()
mod1int = smf.ols('sigma ~ alternancias', data=dfint).fit()
# print(mod.summary())
coefs = mod1.params
coefsint = mod1int.params
#%%
def lineal(x,b,m):
    return m*x+b

plt.figure()
plt.plot(domnorm, sigmas[::-1], '.')
plt.plot(domnorm, lineal(domnorm, *coefsint), 'r-', alpha=0.6)
plt.hlines(0.5, min(domnorm), max(domnorm), ls='dashed', label=r'$0.5$')
plt.legend(loc="upper right")
plt.xlabel(r'Alternancias promedio')
plt.ylabel(r'$\sigma_{NB}$')
plt.savefig('RLas_alt_prom_%iagentes_%ibloques_amean%.1f_con_pers.npz'%(n, agentes, len(bloques), alpha_prom), dpi=400)
#%%
plt.figure()

plt.plot(domnorm, sigmas_int[::-1], '.')
plt.plot(domnorm, lineal(domnorm, *coefsint), 'r-', alpha=0.6)
plt.hlines(0.5, min(domnorm), max(domnorm), ls='dashed', label=r'$0.5$')
plt.ylim(0.3, 1)
plt.legend(loc="upper right")
plt.xlabel(r'Alternancias promedio')
plt.ylabel(r'$\sigma_{NB}$')
plt.savefig('int_RLsim_alt_prom_%iagentes_%ibloques_amean%.1f_con_pers.npz'%(agentes, len(bloques), alpha_prom), dpi=400)
#%%
plt.plot(dif, cint[:,100],'.')
plt.xlabel('dificultad')
plt.ylabel('confianza')
h = plt.hist2d(domnorm, sigmas[::-1], bins=(50, 50), cmap=plt.cm.jet, vmin=0, vmax=40)

cbar = plt.colorbar(h[3])
# cbar.set_label('Confianza', rotation=0)
plt.plot(domnorm, lineal(domnorm, coefs[0], coefs[1]), 'w-', alpha=0.6)
cbar.ax.set_title('Concentración')
# plt.hexbin(dom,sigmas_int, vmin=0, vmax=8)
# plt.xlabel(r'$\Delta \alpha$')
# plt.ylabel(r'$\sigma_{\int}$')
plt.hlines(0.5, min(domnorm), max(domnorm), color='w', alpha=0.95, ls='dashed')
plt.xlabel(r'$Exploración$')
plt.ylabel(r'$\sigma$')
plt.ylim(0.3, 1)
# plt.xlim(4,30)
plt.savefig('color_alt_sigmas_RLsim_%iagentes_%ibloques_amean%.1f_con_pers.png'%(agentes, len(bloques), alpha_prom), dpi=400)

#%%
h = plt.hist2d(domnorm, sigmas_int[::-1], bins=(50, 50), cmap=plt.cm.jet, vmin=0, vmax=40)

cbar = plt.colorbar(h[3])
# cbar.set_label('Confianza', rotation=0)
plt.plot(domnorm, lineal(domnorm, coefsint[0], coefsint[1]), 'w-', alpha=0.6)
cbar.ax.set_title('Concentración')
# plt.hexbin(dom,sigmas_int, vmin=0, vmax=8)
# plt.xlabel(r'$\Delta \alpha$')
# plt.ylabel(r'$\sigma_{\int}$')
plt.hlines(0.5, min(domnorm), max(domnorm), color='w', alpha=0.95, ls='dashed')
plt.xlabel(r'$Exploración$')
plt.ylabel(r'$\sigma$')
plt.ylim(0.3, 1)
# plt.xlim(4,30)
plt.savefig('color_alt_sigmas_RLas_%iagentes_%ibloques_amean%.1f_con_pers.png'%(agentes, len(bloques), alpha_prom), dpi=400)

#%%
plt.figure()
plt.plot(dif_alphas, sigmas_int[::-1], '.')
# plt.plot(dif_alphas, lineal(dif_alphas, *coefsint), 'r-', alpha=0.6)
# h=plt.hist2d(dif_alphas, sigmas, bins=(50, 50), cmap=plt.cm.jet, vmin=0, vmax=40)
cbar = plt.colorbar(h[3])
# cbar.set_label('Confianza', rotation=0)
cbar.ax.set_title('Concentración')
plt.hlines(0.5, dif_alphas[0], dif_alphas[-1], ls='dashed', color='w', label=r'$0.5$')
# plt.legend(loc="upper right")
plt.xlabel(r'$\Delta \alpha$')
plt.ylabel(r'$\sigma$')
plt.ylim(0.4,1)
# plt.savefig('color_dif_alphas_sigmas_bayes_%iagentes_%ibloques_amean%.1f_con_pers.npz'%(n, agentes, len(bloques), alpha_prom), dpi=400)


#%%
df = pd.DataFrame(data=np.matrix([dif_alphas,sigmas]).T, columns=['alternancias','sigma'])
dfint = pd.DataFrame(data=np.matrix([dif_alphas,sigmas_int]).T, columns=['alternancias','sigma'])
mod1 = smf.ols('sigma ~ alternancias', data=df).fit()
mod1int = smf.ols('sigma ~ alternancias', data=dfint).fit()
# print(mod.summary())
coefs = mod1.params
coefsint = mod1int.params
#%%
model = np.polyfit(dif_alphas, sigmas[::-1], 1)

plt.figure()
# plt.plot(dif_alphas, sigmas_int[::-1], '.')
h=plt.hist2d(dif_alphas, sigmas_int[::-1], bins=(50, 50), cmap=plt.cm.jet, vmin=0, vmax=40)
plt.plot(dif_alphas, lineal(dif_alphas, model[1],model[0]), 'w-', alpha=0.6)
plt.hlines(0.5, dif_alphas[0], dif_alphas[-1], ls='dashed', color='w', label=r'$0.5$')
cbar = plt.colorbar(h[3])
# cbar.set_label('Confianza', rotation=0)
cbar.ax.set_title('Concentración')
# plt.plot(dif_alphas, sigmas, '.')
# plt.legend(loc="upper right")
plt.xlabel(r'$\Delta \alpha$')
plt.ylabel(r'$\sigma$')
plt.savefig('color_RLas_%ibloques_amean%i_n%i_%iagentes.png'%(len(bloques), alpha_prom, n, agentes), dpi=400)
# # # #%%
# # modelint = np.polyfit(dif_alphas, sigmas, 1)

# # plt.figure()
# # plt.plot(dif_alphas, sigmas_int, '.', zorder=0)
# # plt.plot(dif_alphas, lineal(dif_alphas, modelint[1],modelint[0]), 'r-', alpha=0.6, zorder=5)
# # plt.hlines(0.5, dif_alphas[0], dif_alphas[-1], ls='dashed', label=r'$0.5$', zorder=10)
# # plt.legend(loc="upper right")
# # plt.xlabel(r'$\Delta \alpha$')
# # plt.ylabel(r'$\sigma$')
# # # plt.savefig('int_RLas_%ibloques_prior_alpha_mean%i_n%i_%iagentes.png'%(len(bloques), alpha_prom, n, agentes), dpi=400)
# #%%
# model = np.polyfit(dif_alphas, domnorm, 1)

# plt.figure()
# plt.plot(dif_alphas, domnorm, '.', ms=0.5)
# plt.plot(dif_alphas, lineal(dif_alphas,model[1],model[0]), '--', color='k', alpha=0.9)
# # plt.hlines(0.5, min(dom), max(dom), ls='dashed', label=r'$0.5$')
# # plt.legend(loc="upper right")
# plt.ylabel(r'Exploración')
# plt.xlabel(r'$\Delta \alpha$')
# # plt.ylim(-.2, .2)
# plt.grid()
# # plt.savefig('RLas_dif_alphas_vs_alt_prom_%ibloques_alpha_prom%i_n%i_%iagentes.png'%(len(bloques), alpha_prom, n, agentes), dpi=400)

# # corr, p = pearsonr(domnorm, dif_alphas)
#%%

persprom = pers.mean(axis=0)
persnorm = persprom/np.max(persprom)

df = pd.DataFrame(data=np.matrix([persnorm,sigmas[::-1]]).T, columns=['alternancias','sigma'])
dfint = pd.DataFrame(data=np.matrix([persnorm,sigmas_int[::-1]]).T, columns=['alternancias','sigma'])
mod1 = smf.ols('sigma ~ alternancias', data=df).fit()
mod1int = smf.ols('sigma ~ alternancias', data=dfint).fit()
# print(mod.summary())
coefs = mod1.params
coefsint = mod1int.params


plt.figure()
h=plt.hist2d(persnorm, sigmas_int[::-1], bins=(50, 50), cmap=plt.cm.jet, vmin=0, vmax=40)
plt.plot(persnorm, lineal(persnorm,*coefsint), 'w-', alpha=0.6)
cbar = plt.colorbar(h[3])
# cbar.set_label('Confianza', rotation=0)
cbar.ax.set_title('Concentración')
# plt.plot(persprom, sigmas_int, '.')
# plt.plot(pers[0,:], lineal(pers[0,:], *coefsint), 'r-', alpha=0.6)
plt.hlines(0.5, min(persnorm), max(persnorm), color='w', ls='dashed', label=r'$0.5$')
# plt.legend(loc="upper right")
plt.xlabel(r'$Explotación$')
plt.ylabel(r'$\sigma$')
plt.ylim(0.3, 1)
plt.show()
plt.savefig('color_pers_sigmas_RLas_%ibloques_alpha_prom%.1f_n%i_%iagentes.png'%(len(bloques), alpha_prom, n, agentes), dpi=400)
#%%
# # from scipy.stats import pearsonr 

# # c = np.sort(cint[0,:])
# # dif = cok[0,:][np.argsort(cint[0,:])]

# # mitadc = int(len(c)/2)
# # c1 = c[10:mitadc]
# # dif1 = dif[10:mitadc]

# # corr, p = pearsonr(c1, dif1)

# # bloque = 0

# # corrok, pok = pearsonr(dom, sigmas)

# # corrok, pok = pearsonr(dom, sigmas_int)

# # corrint, pint = pearsonr(cint[bloque,:], sigmas_int)

# # corrpers, ppers = pearsonr(persprom, sigmas_int)
# # #%%

# # # arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-desiciones/Datos_simulados/dificultad_comportamiento_RL_N100_2agentes_5488bloques_con_pers')

# # # ex, cok, cint, pers, pok, pnb, dif_alphas, sigmas, sigmas_int, bloques = [arrays['arr_%i'%i] for i in range(10)]

# # agente = 9
# # atr = a.flatten()
# # dificultad = dif.flatten()

# # # ax = plt.axes(projection='3d')
# # plt.scatter(x=atr, y=dificultad, c=cint[:,agente], cmap='viridis')#, vmin=0, vmax=1, linewidth=0.5)

# # # plt.plot(persprom, sigmas_int, '.')
# # # plt.plot(pers[0,:], lineal(pers[0,:], *coefsint), 'r-', alpha=0.6)
# # # plt.hlines(0.5, min(persnorm), max(persnorm), color='w', ls='dashed', label=r'$0.5$')
# # # plt.legend(loc="upper right")
# # plt.xlabel(r'Dificultad')
# # plt.ylabel(r'Atractivo')
# # # plt.clabel(r'Confianza')
# # plt.colorbar()
# # plt.show()
# # #%%
# # from matplotlib import cm
# # from matplotlib.ticker import LinearLocator
# # from mpl_toolkits.mplot3d import Axes3D
# # from scipy.interpolate import griddata

# # fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

# # # Make data.
# # x = atr
# # y = dificultad

# # # construcción de la grilla 2D
# # xi = np.linspace(min(x), max(x))
# # yi = np.linspace(min(y), max(y))
# # X, Y = np.meshgrid(xi, yi)
# # z = cint[:,agente]
 

# # # interpolación
# # #Z = griddata(x, y, z, xi, yi, interp='linear')
# # Z = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')

# # ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,linewidth=1, antialiased=True)

