#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  5 19:05:01 2021

@author: Tomas
"""


import pymc3 as pm
import numpy as np
import theano.tensor as tt
import scipy.stats as stats
from tqdm import tqdm 
import theano
import copy
from scipy.integrate import quad

#defino las funciones para el modelo RW asimétrico

def update_Q(action, reward,
             Qs,
             alphas):
    """
    This function updates the Q table according to the RL update rule.
    It will be called by tt.scan to do so recursevely, given the observed data and the alpha parameter
    This could have been nlaced be the following lamba expression in the tt.scan fn argument:
        fn=lamba action, reward, Qs, alpha: tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    """
    Qs = tt.set_subtensor(Qs[action], Qs[action] + alphas[reward] * (reward - Qs[action]))
    return Qs

# Transform the variables into appropriate Theano objects
def theano_llik_td(alphas, beta, actions, rewards):
    rewards = theano.shared(np.asarray(rewards, dtype='int16'))
    actions = theano.shared(np.asarray(actions, dtype='int16'))

    # Compute the Qs values
    Qs = 0.5 * tt.ones((2), dtype='float64')
    Qs, updates = theano.scan(
        fn=update_Q,
        sequences=[actions, rewards],
        outputs_info=[Qs],
        non_sequences=[alphas])#[var]

    # Apply the sotfmax transformation
    Qs_ = Qs[:-1] * beta
    log_prob_actions = Qs_ - pm.math.logsumexp(Qs_, axis=1)

    # Calculate the negative log likelihod of the observed actions
    log_prob_actions = log_prob_actions[tt.arange(actions.shape[0]-1), actions[1:]]
    return tt.sum(log_prob_actions)  # PyMC makes it negative by default

def generate_data_RLas(alpha_s, alpha_f, beta, n=100, p_r=[.4, .6]):
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    Qs = np.zeros((n, 2))

    # Initialize Q table
    Q = np.array([.5, .5])
    for i in range(n):
        # Apply the Softmax transformation
        exp_Q = np.exp(beta*Q)
        prob_a = exp_Q / np.sum(exp_Q)

        # Simulate choice and reward
        a = np.random.choice([0, 1], p=prob_a)
        r = np.random.rand() < p_r[a]

        # Update Q table
        if r:
            Q[a] = Q[a] + alpha_s * (r - Q[a])
            # print('r=1')
        else:
            Q[a] = Q[a] + alpha_f * (r - Q[a])
            # print('r=0')

        # Store values
        actions[i] = a
        rewards[i] = r
        Qs[i] = copy.deepcopy(Q) # copio para no sobreescribir la variable

    return actions, rewards, Qs

def generate_data_bayes(priorA, priorB, n=100, p_r=[.4, .6]):
    # paso de b,w a e,f
    expA = np.array([priorA[0]*priorA[1], priorA[1]-priorA[0]*priorA[1]]) #[e, f]
    expB = np.array([priorB[0]*priorB[1], priorB[1]-priorB[0]*priorB[1]]) #[e, f]
    
    #experiencia verdadera (sin prior)
    expAr = np.array([0, 0]) #[e, f]
    expBr = np.array([0, 0]) #[e, f]
    
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    
    Ps = np.zeros((n, 2))
    
    for i in range(n):
        # print(i)
        Ps[i] = np.array([(expA[0]+1)/(np.sum(expA)+2), (expB[0]+1)/(np.sum(expB)+2)])
        # Simulate choice and reward
        
        #eleccion de maquina 0 o 1
        if stats.beta.rvs(*expA,1)>stats.beta.rvs(*expB,1):
            eleccion=0
        else:
            eleccion=1    
        resultado = np.random.rand() < p_r[eleccion]
        
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            expA[0]=expA[0]+1
            expAr[0]=expAr[0]+1
        elif eleccion==0 and resultado==0:
            expA[1]=expA[1]+1
            expAr[1]=expAr[1]+1
        elif eleccion==1 and resultado==1:
            expB[0]=expB[0]+1
            expBr[0]=expBr[0]+1
        elif eleccion==1 and resultado==0:
            expB[1]=expB[1]+1
            expBr[1]=expBr[1]+1


        # Store values
        actions[i] = eleccion
        rewards[i] = resultado

    return actions, rewards, Ps, expA, expB, expAr, expBr

def integrandB(x, eA, fA, eB, fB):
    return stats.beta.pdf(x, eA+1, fA+1)*(1-stats.beta.cdf(x, eB+1, fB+1))

def confidence(parA, parB):
    # print(parA)
    # print(parB)
    
    IA = quad(integrandB, 0, 1, args=(*parB, *parA))[0]
    IB = quad(integrandB, 0, 1, args=(*parA, *parB))[0]
    
    # print(IA, IB)
    
    N = np.sum([IA, IB])
    
    res = [IA/N, IB/N]
    
    return np.array(res)
#%%
n = 1000
npar = 2

trials = np.arange(1,n+1,50)

agentes = 3
# defino los parámetros de los agentes (recorro toda la grilla)
bs = np.linspace(0.05,0.95,agentes)
x = np.linspace(0.05,0.95,1000)

w_fit = 50

total = 1

#probabilidades de pago verdaderas

p_r =[.2, .6]#, [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7], [.2, .8]]

np_r = 1#len(p_r_list) # número de escenarios - 1

   
dataprom = np.zeros((npar,2,len(trials),agentes))

resultados = np.zeros((n,2,agentes))

for agente in tqdm(range(agentes), desc='Loop principal'):
    prior = [bs[agente], w_fit]

    data = np.zeros((npar,2,len(trials)))
            
    actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(prior, prior, n=n, p_r=p_r)
    
     #inicializo las listas
    data = np.array((actions, rewards)).T
    
    resultados[:,:,agente] = data

    p_prob = np.zeros((len(x), len(trials)))
    prob = np.zeros((len(x), len(trials)))
    
    #experiencia verdadera (sin prior)
    s0, f0, s1, f1 = np.zeros((4))
    
    aux = -1
    
    for l in tqdm(range(n), desc='Loop trials'):
        # print(l)
        eleccion = actions[l]
        resultado = rewards[l]
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        
        if eleccion==0 and resultado==1:
            s0=s0+1
        elif eleccion==0 and resultado==0:
            f0=f0+1
        elif eleccion==1 and resultado==1:
            s1=s1+1
        elif eleccion==1 and resultado==0:
            f1=f1+1
        
        if l in trials:
            aux += 1    
            #llamo b_fit al unico parametros libre (asumo b0=b1 y w verdadero)
            for i_0, b_fit in enumerate(x):
                
                # print('b='+str(b_fit)+'\nw='+str(w_fit))
                  
                s0_p = b_fit*w_fit
                f0_p = (w_fit-b_fit*w_fit)
                
                s1_p = b_fit*w_fit
                f1_p = (w_fit-b_fit*w_fit)
                
                # print(s0_p+s0_total,f0_p+f0)
                # print(s1_p+s1,f1_p+f1)
                # print(l, b_fit)
                
                s0_real = s0_p + s0
                f0_real = f0_p + f0
                s1_real = s1_p + s1
                f1_real = f1_p + f1
                
                #computo pdf's y cdf's
                prob0, prob1 = confidence([s0_real, f0_real], [s1_real,f1_real])
                
                # print('prob0='+str(prob0))
                # print('prob1='+str(prob1))
                
                #probabilidad de la eleccion dado el prior (labeled by i_0)
                if eleccion==0:
                    p_prob[i_0,aux]=np.log(prob0)
                elif eleccion==1:
                    p_prob[i_0,aux]=np.log(prob1)
               
                #i.i.d. + np.log sapce
                prob[i_0,aux]=np.sum(p_prob[i_0,1:aux])
                   
            #normalizo y paso a linear space
                        
            prob[:,aux]=np.exp(prob[:,aux])/np.sum(np.exp(prob[:,aux]))
            # print(prob[:,aux])
            # print(np.sum(prob[:,aux]))
            # print('no se ve nada')
          
            dataprom[0,:,aux,agente] = [np.average(x, weights=prob[:,aux]), np.sqrt(np.cov(x, aweights=prob[:,aux]))]
            dataprom[1,:,aux,agente] = [w_fit, 0]# [np.average(ws, weights=prob[:,-1]), np.sqrt(np.cov(ws, aweights=prob[:,-1]))]
    
np.savez('bayes_bayes_N%i_%i_esc(%i rep)_.2, .6_w8b%.1f.npz'%(n, np_r, total, prior[0]), dataprom, resultados)
#%%
from matplotlib import pyplot as plt

# arrays = np.load('bayes_bayes_N1000_1_esc(1 rep)_.2, .6_w8b0.9grid1000.npz')

# dataprom, resultados = [arrays['arr_%i'%i] for i in range(2)]

parfit = dataprom[:,0,:]
errpar = dataprom[:,1,:]/np.sqrt(total)

npar = 0

for agente in range(agentes):
           
    plt.figure()
       
    plt.plot(trials, parfit[npar,:,agente],'.', color = 'C%i'%1, alpha=0.85, ms=5, label=r"fit")
    plt.errorbar(trials, parfit[npar,:,agente], errpar[npar,:,agente], fmt='none', ecolor = 'C%i'%1, alpha=0.5)
    # plt.plot(alphas, alphas, 'k--', alpha=0.3)
    #plt.xticks(ticks=dom, labels=p_r_list)
    plt.hlines(bs[agente], trials[0], trials[-1], ls='dashed',label=r'$b_{true}$')
    # if i==1:
    # plt.hlines(alfa_menos, trials[0], trials[-1], ls='dashed', color='k',alpha=0.2,label=r'%.2f'%alfa_menos)
    # if i==2:
    # plt.hlines(alfa_mas, trials[0], trials[-1], ls='dashed', color='k',alpha=0.2,label=r'%.2f'%alfa_mas)
    # if i!=0:
    plt.ylim(0,1)
    # plt.legend(loc="upper right")
    plt.xlabel(r'trials $N$')
    plt.ylabel(r'$b_{fit}$')
    # plt.ylabel(r'$\alpha$')
    plt.title(r"$b_{true} = %.2f$. $N=%i$ (%i rep)"%(bs[agente], n, total))
    plt.grid()
    plt.savefig('bayes_convergence_b%.2f.png'%bs[agente], dpi=300)