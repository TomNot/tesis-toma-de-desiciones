#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 23:39:12 2021

@author: Tomas
"""


from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm, tnrange
import seaborn as sns
import theano
import arviz as az
import copy 
import arviz as az
from scipy.integrate import quad

dim = 50

a = np.linspace(0.05, 0.95, dim)
# a = (A+B)/2

d = np.linspace(0.05, 0.95, dim)
# d = |A-B|
# supongamos que A > B entonces d = A-B

a,d = np.meshgrid(a,d)

A = a + d/2

B = a - d/2

print(A)
print(B)

#%%
Abool = np.zeros((dim,dim))
for i, row in enumerate(A):
    for j, column in enumerate(row):
        Abool[i,j] = 0<column<1
print(Abool)

Bbool = np.zeros((dim,dim))
for i, row in enumerate(B):
    for j, column in enumerate(row):
        Bbool[i,j] = 0<column<1
print(Bbool)                

aux = np.abs(Abool - Bbool)
aux = np.abs(aux - 1)

Aok = np.zeros((dim,dim))
Bok = np.zeros((dim,dim))

for i, row in enumerate(aux):
    for j, column in enumerate(row):
        Aok[i,j]=aux[i,j]*A[i,j]
        Bok[i,j]=aux[i,j]*B[i,j]

print(Aok)
print(Bok)
#%%
bloques = []
for i, row in enumerate(aux):
    for j, column in enumerate(row):
        if Aok[i,j]!=0:
            bloques.append([Aok[i,j], Bok[i,j]])
print(bloques)
#%%
bloques = np.array(bloques)
A = bloques[:,0]
B = bloques[:,1]

a = (A+B)/2
d = A-B
dif = 1 - d
#%%

arrays = np.load('dificultad_comportamiento_bayes_N100_6agentes_1348bloques_con_pers.npz')

# ex, cok, cint, pers, sigmas, sigmas_int, dif_alphas, bloques = [arrays['arr_%i'%i] for i in range(8)]
ex, cok, cint, pers, pok, pnb, bs, sigmas, sigmas_int, bloques = [arrays['arr_%i'%i] for i in range(10)]
cok = cok.mean(axis=2)

# cok = np.abs(cok)
# cok = 0.5*cok + 0.5
cint = cint.mean(axis=2)

agentes = 6
for agente in range(agentes):
    
    nivel = -np.log(cint[:,agente])
    
    # agente = agentes-1
    atr = a.flatten()
    dificultad = dif.flatten()
    
    plt.figure()
    # ax = plt.axes(projection='3d')
    plt.scatter(x=atr, y=dificultad, c=cint[:,agente], cmap='viridis')#, vmin=0.5, vmax=1, linewidth=0.5)
    # plt.plot(persprom, sigmas_int, '.')
    # plt.plot(pers[0,:], lineal(pers[0,:], *coefsint), 'r-', alpha=0.6)
    # plt.hlines(0.5, min(persnorm), max(persnorm), color='w', ls='dashed', label=r'$0.5$')
    # plt.legend(loc="upper right")
    plt.ylabel(r'Dificultad')
    plt.xlabel(r'Atractivo')
    # plt.clabel(r'Confianza')
    cbar = plt.colorbar()
    # cbar.set_label('Confianza', rotation=0)
    cbar.ax.set_title('Confianza')
    # plt.title(r'$\Delta \alpha=%.2f$'%dif_alphas[agente])
    plt.title(r'$b%.2f$'%bs[agente])
    plt.show()
    plt.savefig('isoconfianza_agente%i.png'%agente, dpi=400)
#%% GIF 

import imageio

# Build GIF
with imageio.get_writer('mygif.gif', mode='I', fps=1) as writer:
    for filename in ['isoconfianza_agente%i.png'%agente for agente in range(agentes)]:
        image = imageio.imread(filename)
        writer.append_data(image)
#%% Filtro los valores con la misma confianza aprox
        
import statsmodels.formula.api as smf  ## Permite ajustar modelos estadísticos utilizando fórmulas de estilo R

def lineal(x, m, b):
    return m*x + b

def filtro_conf(conf, cmin, cmax):
    fconf = []
    argconf = []
    for i in range(len(conf)):
        if cmin <= conf[i] <= cmax:
            # print(conf[i])
            fconf.append(conf[i])
            argconf.append(i)
    fconf = np.array(fconf)
    argconf = argconf
    return argconf,fconf

agentes=6

cok = np.abs(cok)

nlines = int(0.9/0.02)

coefs = np.zeros((len(bs), 2, nlines))


for agente in tqdm(range(len(bs))):
    centro = 0.65#np.mean(cint[:,agente])
    oos = [centro + 0.05*i for i in range(int(3))] 
    for i, oo in enumerate(oos):
   
        argconf, fconf = filtro_conf(cint[:,agente], oo, oo+0.02) 
        # print(argconf)
        coef = np.polyfit(atr[argconf], dif[argconf], 1)
        coefs[agente,:,i] = coef
     
        # print(coef)

    plt.figure()
    # ax = plt.axes(projection='3d')
    # plt.scatter(x=atr[argconf], y=dificultad[argconf], c=cint[argconf,agente], cmap='viridis')#, vmin=0.5, vmax=1, linewidth=0.5)
    plt.scatter(x=atr, y=dificultad, c=cint[:,0], cmap='viridis')#, vmin=0.5, vmax=1, linewidth=0.5)
    for i in range(len(oos)):
        plt.plot(atr, lineal(atr, *coefs[agente,:,i]), 'r', ls = 'dashed', lw = 0.5, label='isoconfianza' )
    # plt.plot(persprom, sigmas_int, '.')
    # plt.plot(pers[0,:], lineal(pers[0,:], *coefsint), 'r-', alpha=0.6)
    # plt.hlines(0.5, min(persnorm), max(persnorm), color='w', ls='dashed', label=r'$0.5$')
    # plt.legend(loc="upper right")
    plt.ylabel(r'Dificultad')
    plt.xlabel(r'Atractivo')
    # plt.clabel(r'Confianza')
    cbar = plt.colorbar()
    # cbar.set_label('Confianza', rotation=0)
    cbar.ax.set_title('Confianza')
    plt.title(r'$b%.2f$'%bs[agente])
    # plt.title(r'$\Delta \alpha=%.2f$'%dif_alphas[agente])
    plt.show()
        