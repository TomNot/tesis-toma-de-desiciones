#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 18:12:29 2021

@author: Tomas
"""
#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Jan  5 13:42:38 2021

@author: Tomas
"""


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 19:25:37 2020

@author: Tomas
"""
from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm, tnrange
import seaborn as sns
import theano
import arviz as az
import copy 
#%% Asymmetrical Rescorla-Wagner

# Defino una función que plotea los resultados de la simulación

def plot_data(actions, rewards, Qs):
    plt.figure(figsize=(20,3))
    x = np.arange(len(actions))

    plt.plot(x, Qs[:, 0] - .5 + 0, c='C0', lw=3, alpha=.3)
    plt.plot(x, Qs[:, 1] - .5 + 1, c='C1', lw=3, alpha=.3)

    s = 50
    lw = 2

    cond = (actions == 0) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C0', lw=lw)

    cond = (actions == 0) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C0', ec='C0', lw=lw)

    cond = (actions == 1) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C1', lw=lw)

    cond = (actions == 1) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C1', ec='C1', lw=lw)

    plt.scatter(0, 20, c='k', s=s, lw=lw, label='Reward')
    plt.scatter(0, 20, c='w', ec='k', s=s, lw=lw, label='No reward')
    plt.plot([0,1], [20, 20], c='k', lw=3, alpha=.3, label='Qvalue (centered)')


    plt.yticks([0,1], ['left', 'right'])
    plt.ylim(-1, 2)

    plt.ylabel('action')
    plt.xlabel('trial')

    handles, labels = plt.gca().get_legend_handles_labels()
    order = (1,2,0)
    handles = [handles[idx] for idx in order]
    labels = [labels[idx] for idx in order]

    plt.legend(handles, labels, fontsize=12, loc=(1.01, .27))
    plt.tight_layout()

# Genero data 
    
def generate_data(alpha_s, alpha_f, beta, n=100, p_r=[.4, .6]):
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    Qs = np.zeros((n, 2))

    # Initialize Q table
    Q = np.array([.5, .5])
    for i in range(n):
        # Apply the Softmax transformation
        exp_Q = np.exp(beta*Q)
        prob_a = exp_Q / np.sum(exp_Q)

        # Simulate choice and reward
        a = np.random.choice([0, 1], p=prob_a)
        r = np.random.rand() < p_r[a]

        # Update Q table
        if r:
            Q[a] = Q[a] + alpha_s * (r - Q[a])
            # print('r=1')
        else:
            Q[a] = Q[a] + alpha_f * (r - Q[a])
            # print('r=0')

        # Store values
        actions[i] = a
        rewards[i] = r
        Qs[i] = copy.deepcopy(Q) # copio para no sobreescribir la variable

    return actions, rewards, Qs


true_alpha_s = .1
true_alpha_f = .3
true_beta = 5

#número de trials
n = 100
#probabilidades de pago verdaderas
p_r=[.8, .9]

actions, rewards, Qs = generate_data(true_alpha_s, true_alpha_f, true_beta, n, p_r=p_r)

plot_data(actions, rewards, Qs)

alphas = []
for r in rewards:
    if r:
        alphas.append(true_alpha_s)
    else:
        alphas.append(true_alpha_f)

#%%
def update_Q(action, reward,
             Qs,
             alphas):
    """
    This function updates the Q table according to the RL update rule.
    It will be called by tt.scan to do so recursevely, given the observed data and the alpha parameter
    This could have been replaced be the following lamba expression in the tt.scan fn argument:
        fn=lamba action, reward, Qs, alpha: tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    """
    Qs = tt.set_subtensor(Qs[action], Qs[action] + alphas[reward] * (reward - Qs[action]))
    return Qs

#%%
# Transform the variables into appropriate Theano objects
def theano_llik_td(alphas, beta, actions, rewards):
    rewards = theano.shared(np.asarray(rewards, dtype='int16'))
    actions = theano.shared(np.asarray(actions, dtype='int16'))

    # Compute the Qs values
    Qs = 0.5 * tt.ones((2), dtype='float64')
    Qs, updates = theano.scan(
        fn=update_Q,
        sequences=[actions, rewards],
        outputs_info=[Qs],
        non_sequences=[alphas])#[var]

    # Apply the sotfmax transformation
    Qs_ = Qs[:-1] * beta
    log_prob_actions = Qs_ - pm.math.logsumexp(Qs_, axis=1)

    # Calculate the negative log likelihod of the observed actions
    log_prob_actions = log_prob_actions[tt.arange(actions.shape[0]-1), actions[1:]]
    return tt.sum(log_prob_actions)  # PyMC makes it negative by default


# #%%
# n = 100
# npar = 3
# trials = np.arange(1,n+1,10)

# Rep = len(trials)

# total = 1 #cambiar el tqdm de for

# dataprom = np.zeros((npar,2,Rep,total))

# for k in tqdm(range(total), desc='1st loop'):
#     data = np.zeros((npar,2,Rep))
#     i=-1
#     for h_trial in tqdm(trials, desc='2nd loop'):
#         i+=1
#         actions, rewards, Qs = generate_data(true_alpha_s, true_alpha_f, true_beta, n, p_r=p_r)
        
#         with pm.Model() as m:
#             # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
#             # beta = pm.Exponential('beta', 10, testval=5)
            
#             # funciona mejor con estos priors que con los anteriores
            
#             alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([.1, .3])) #empiezo de una uniforme
#             beta = pm.HalfNormal('beta', sigma=1, testval=5)
            
            
#             # pm.Potential me permite definir "distribuciones" arbitrarias
            
#             like = pm.Potential('like', theano_llik_td(alphas, beta, actions, rewards))
        
#             # trace = pm.sample() # sampleo por defecto
#             # burned_trace=trace[1000:]
            
#             # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
#                                                                 # era para arreglar un error
            
#             step = pm.Metropolis()
#             trace = pm.sample(20000, step=step)
#             burned_trace=trace[1000:]
            
#         par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
#         data[:,:,i] = par_fit
#         print(data[:,:,i])
#     dataprom[:,:,:,k] = data
    
# data = dataprom.mean(axis=3)
# np.savez('RLas_RLas_N100_20rep.npz', data)
# #%%
# data = np.load('RLas_RLas_N100_20rep.npz')['arr_0']
# figsize(10, 8)

# #histogram of alphas

# ax = plt.subplot(111)

# plt.xlim(0, n+1)
# plt.plot(trials, data[1,0,:]-data[0,0,:],'r.', alpha=0.85, label='ajuste')
# plt.hlines(true_alpha_s-true_alpha_f, trials[0], n+1, ls='dashed', label=r'true $\Delta \alpha$')
# # plt.plot(alphas, alphas, 'k--', alpha=0.3)
# plt.legend(loc="upper right")
# plt.xlabel(r'$N$')
# plt.ylabel(r'$\Delta \alpha$ fit')
# plt.title(r"$\Delta \alpha$'s fiteados vs trials. $N=%i$ (sin rep)"%n)
# plt.savefig('RLas_RLas_N100_20rep.png', dpi=400)
#%%
# valores de los alfas
true_alpha_s = [.1, .2, .3, .4, .9]
true_alpha_f = [.3, .2, .1, .8, .9]

n = 100
npar = 3
trials = np.arange(1,n+1,10)

Rep = len(trials)

total = 20 #cambiar el tqdm de for


for t_alpha_s, t_alpha_f in tqdm(zip(true_alpha_s, true_alpha_f), desc='Ppal. loop'):
    dataprom = np.zeros((npar,2,Rep,total))
    for k in tqdm(range(total), desc='1st loop'):
        data = np.zeros((npar,2,Rep))
        i=-1
        for h_trial in tqdm(trials, desc='2nd loop'):
            i+=1
            actions, rewards, Qs = generate_data(t_alpha_s, t_alpha_f, true_beta, n, p_r=p_r)
            
            with pm.Model() as m:
                # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
                # beta = pm.Exponential('beta', 10, testval=5)
                
                # funciona mejor con estos priors que con los anteriores
                
                alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([.1, .3])) #empiezo de una uniforme
                beta = pm.HalfNormal('beta', sigma=1, testval=5)
                
                
                # pm.Potential me permite definir "distribuciones" arbitrarias
                
                like = pm.Potential('like', theano_llik_td(alphas, beta, actions, rewards))
            
                # trace = pm.sample() # sampleo por defecto
                # burned_trace=trace[1000:]
                
                # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                                    # era para arreglar un error
                
                step = pm.Metropolis()
                trace = pm.sample(20000, step=step)
                burned_trace=trace[1000:]
                
            par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
            data[:,:,i] = par_fit
            print(data[:,:,i])
        dataprom[:,:,:,k] = data
        
    data = dataprom.mean(axis=3)
    np.savez('RLas_RLas_N100_20rep_as%.2f_af%.2f.npz'%(t_alpha_s, t_alpha_f), data)