#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 22 23:49:02 2021

@author: Tomas
"""


from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm, tnrange
import seaborn as sns
import theano
import arviz as az
import copy 
import arviz as az
from scipy.integrate import quad

dim = 50

a = np.linspace(0.05, 0.95, dim)
# a = (A+B)/2

d = np.linspace(0.05, 0.95, dim)
# d = |A-B|
# supongamos que A > B entonces d = A-B

a,d = np.meshgrid(a,d)

A = a + d/2

B = a - d/2

print(A)
print(B)

#%%
Abool = np.zeros((dim,dim))
for i, row in enumerate(A):
    for j, column in enumerate(row):
        Abool[i,j] = 0<column<1
print(Abool)

Bbool = np.zeros((dim,dim))
for i, row in enumerate(B):
    for j, column in enumerate(row):
        Bbool[i,j] = 0<column<1
print(Bbool)                

aux = np.abs(Abool - Bbool)
aux = np.abs(aux - 1)

Aok = np.zeros((dim,dim))
Bok = np.zeros((dim,dim))

for i, row in enumerate(aux):
    for j, column in enumerate(row):
        Aok[i,j]=aux[i,j]*A[i,j]
        Bok[i,j]=aux[i,j]*B[i,j]

print(Aok)
print(Bok)
#%%
bloques = []
for i, row in enumerate(aux):
    for j, column in enumerate(row):
        if Aok[i,j]!=0:
            bloques.append([Aok[i,j], Bok[i,j]])
print(bloques)
#%%

def generate_data_bayes(priorA, priorB, n=100, p_r=[.4, .6]):
    # paso de b,w a e,f
    expA = np.array([priorA[0]*priorA[1], priorA[1]-priorA[0]*priorA[1]]) #[e, f]
    expB = np.array([priorB[0]*priorB[1], priorB[1]-priorB[0]*priorB[1]]) #[e, f]
    
    #experiencia verdadera (sin prior)
    expAr = np.array([0, 0]) #[e, f]
    expBr = np.array([0, 0]) #[e, f]
    
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    
    Ps = np.zeros((n, 2))
    
    for i in range(n):
        # print(i)
        Ps[i] = np.array([(expA[0]+1)/(np.sum(expA)+2), (expB[0]+1)/(np.sum(expB)+2)])
        # Simulate choice and reward
        
        #eleccion de maquina 0 o 1
        if stats.beta.rvs(*expA,1)>stats.beta.rvs(*expB,1):
            eleccion=0
        else:
            eleccion=1    
        resultado = np.random.rand() < p_r[eleccion]
        
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            expA[0]=expA[0]+1
            expAr[0]=expAr[0]+1
        elif eleccion==0 and resultado==0:
            expA[1]=expA[1]+1
            expAr[1]=expAr[1]+1
        elif eleccion==1 and resultado==1:
            expB[0]=expB[0]+1
            expBr[0]=expBr[0]+1
        elif eleccion==1 and resultado==0:
            expB[1]=expB[1]+1
            expBr[1]=expBr[1]+1


        # Store values
        actions[i] = eleccion
        rewards[i] = resultado

    return actions, rewards, Ps, expA, expB, expAr, expBr

def integrandB(x, eA, fA, eB, fB):
    return stats.beta.pdf(x, eA+1, fA+1)*(1-stats.beta.cdf(x, eB+1, fB+1))

def confidence(parA, parB):
    # print(parA)
    # print(parB)
    
    IA = quad(integrandB, 0, 1, args=(*parB, *parA))[0]
    IB = quad(integrandB, 0, 1, args=(*parA, *parB))[0]
    
    # print(IA, IB)
    
    N = np.sum([IA, IB])
    
    res = [IA/N, IB/N]
    
    return np.array(res)

def persistencias(actions, rewards):
    n = len(actions)
    pers = 0
    for t in range(n):
        if rewards[t]==0:
            if t<(n-1) and actions[t+1]==actions[t]:
                pers+=1
    return pers

agentes = 6
# defino los parámetros de los agentes (recorro toda la grilla)
bs = np.linspace(0.05,0.95,agentes)
w = 8

# defino la cantidad de trials
n = 100

repeticiones = 20
# inicializo arrays para guardar los resultados
# resultados = np.zeros((n,2,len(bloques),len(bs)))
# Qs = np.zeros((n,2,len(bloques),len(bs)))
# confs = np.zeros((2,len(bloques),len(bs)))
cok = np.zeros((len(bloques),len(bs),repeticiones))
cint = np.zeros((len(bloques),len(bs),repeticiones))
pok = np.zeros((len(bloques),len(bs),repeticiones))
pnb = np.zeros((len(bloques),len(bs),repeticiones))
ex = np.zeros((len(bloques),len(bs),repeticiones))
pers = np.zeros((len(bloques),len(bs),repeticiones))

for j in range(repeticiones):
    for i,p in tqdm(enumerate(bs)):    
        # print(a_s,a_f)
        for b,p_r in tqdm(enumerate(bloques)):
            # print('\n'+'p\'s={}'.format(p_r)+'\n')
            prior = [p,w]
            actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(prior, prior, n=n, p_r=p_r)
            
            cok[b,i,j] = np.diff(Ps[-1])
            Psnb = np.array([(expAr[0]+1)/(np.sum(expAr)+2), (expBr[0]+1)/(np.sum(expBr)+2)])
            # print(Psnb)
            pok[b,i,j] = np.diff(Psnb)
            s0_real, f0_real = expA
            s1_real, f1_real = expB
            
            s0, f0 = expAr
            s1, f1 = expBr
            
            #computo las probabilidades
            prob0, prob1 = confidence([s0_real, f0_real], [s1_real,f1_real])
            prob0nb, prob1nb = confidence([s0, f0], [s1,f1])
            
            # # print('prob0='+str(prob0))
            # # print('prob1='+str(prob1))
            
            eleccion = actions[-1]
            
            # #probabilidad de la eleccion dado el prior 
            if eleccion==0:
                cint[b,i,j] = prob0
                pnb[b,i,j] = prob0nb
            elif eleccion==1:
                cint[b,i,j] = prob1
                pnb[b,i,j] = prob1nb
            ex[b,i,j] = np.sum(np.abs(np.diff(actions)))
            pers[b,i,j] = persistencias(actions,rewards)
        
#%%
bloques = np.array(bloques)
A = bloques[:,0]
B = bloques[:,1]

a = (A+B)/2
d = A-B
dif = 1 - d

#%%
# import statsmodels.api as sm           ## Este proporciona funciones para la estimación de muchos modelos estadísticos
import statsmodels.formula.api as smf  ## Permite ajustar modelos estadísticos utilizando fórmulas de estilo R

def sigmoidea(coefs):
    return 1/(1+np.exp(-coefs[0]/-coefs[1]))

sigmas = np.zeros((len(bs)))
sigmas_int = np.zeros((len(bs)))

caux = cint.mean(axis=2)
cauxok = cok.mean(axis=2)
for i in tqdm(range(len(bs))):
    
    dtint = pd.DataFrame(data=np.matrix([a,dif,caux[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    dt = pd.DataFrame(data=np.matrix([a,dif,cauxok[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    
    mod = smf.ols('confianza ~ atractivo + dificultad', data=dt).fit()
    modint = smf.ols('confianza ~ atractivo + dificultad', data=dtint).fit()
    # print(mod.summary())
    coefs = mod.params
    coefsint = modint.params
    # print(coefs)
    sigma = sigmoidea(coefs[1:])
    sigmaint = sigmoidea(coefsint[1:])
    sigmas[i] = sigma
    sigmas_int[i] = sigmaint
np.savez('dificultad_comportamiento_bayes_N%i_%iagentes_%ibloques_con_pers.npz'%(n, agentes, len(bloques)), ex, cok, cint, pers, pok, pnb, bs, sigmas, sigmas_int, bloques)