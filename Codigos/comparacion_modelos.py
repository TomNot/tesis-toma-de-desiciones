#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 18:47:18 2021

@author: Tomas
"""

# Importo las librerías para graficar
from IPython.core.pylabtools import figsize
import matplotlib.pyplot as plt
import numpy as np

#%% genera R-W asimétrico, predice R-W asimétrico

arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-decisiones/Datos_simulados/Preds_RLas_RLas_N26_4_esc(100 rep).npz')

dataprom = arrays['arr_0']
preds = arrays['arr_1']
resultados = arrays['arr_2']

total = 100 # repeticiones por escenario para promediar

preds_prom_rr = preds.mean(axis=0)
preds_std_rr = preds.std(axis=0, ddof=1)/np.sqrt(total)

# Escenarios de las simuaciones
p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7], [.2, .8]]

np_r = 4 #len(p_r_list)

n = 26 # número de trials

figsize(10, 8)

#BICs en función del escenario

xlabels = ["Fail or Success\n"+str(p_r_list[i]) for i in range(len(preds_prom_rr))]

plt.figure()

ax = plt.subplot(111)

dom = np.arange(0,len(preds_prom_rr))


plt.plot(dom, preds_prom_rr,'r.', alpha=0.85, label=r'Preds')
plt.errorbar(dom, preds_prom_rr, preds_std_rr, fmt='none', ecolor='red')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.xticks(ticks=dom, labels=p_r_list)
plt.legend(loc="upper right")
plt.xlabel(r'Stage $[p_A, p_B]$')
plt.ylabel(r'Frecuencia promedio')
plt.title(r"Preds vs Stage. $N=%i$ (%i rep)"%(n, total))
plt.grid()
#plt.savefig('BIC_vs_Stage_bayes_RLas_N%i_%i_esc(%i rep)'%(n, np_r, total), dpi=400)
#%% genera R-W asimétrico, predice bayes+Thompson sampling

arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-decisiones/Datos_simulados/Preds_RLas_bayes_N26_4_esc(100 rep).npz')

dataprom = arrays['arr_0']
preds = arrays['arr_1']
resultados = arrays['arr_2']

preds_prom_rb = preds.mean(axis=0)
preds_std_rb = preds.std(axis=0, ddof=1)/np.sqrt(total)

# Escenarios de las simuaciones
p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7], [.2, .8]]

np_r = 4#len(p_r_list)

n = 26 # número de trials

total = 100 # repeticiones por escenario para promediar

figsize(10, 8)

#BICs en función del escenario

plt.figure()

ax = plt.subplot(111)

dom = np.arange(0,len(preds_prom_rb))


plt.plot(dom, preds_prom_rb,'r.', alpha=0.85, label=r'Preds RW/B')
#plt.plot(dom, preds_prom_bb,'b.', alpha=0.85, label=r'Preds B/B')
plt.errorbar(dom, preds_prom_rb, preds_std_rb, fmt='none', ecolor='red')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.xticks(ticks=dom, labels=p_r_list)
plt.legend(loc="upper right")
plt.xlabel(r'Stage $[p_A, p_B]$')
plt.ylabel(r'Frecuencia promedio')
plt.title(r"Preds vs Stage. $N=%i$ (%i rep)"%(n, total))
plt.grid()
#%% genera bayes, predice bayes

arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-decisiones/Datos_simulados/Preds_bayes_bayes_N26_4_esc(100 rep).npz')

dataprom = arrays['arr_0']
preds = arrays['arr_1']
resultados = arrays['arr_2']

preds_prom_bb = preds.mean(axis=0)
preds_std_bb = preds.std(axis=0, ddof=1)/np.sqrt(total)

# Escenarios de las simuaciones
p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7], [.2, .8]]

np_r = 4 #len(p_r_list)

n = 26 # número de trials

total = 100 # repeticiones por escenario para promediar

figsize(10, 8)

#BICs en función del escenario

plt.figure()

ax = plt.subplot(111)

dom = np.arange(0,len(preds_prom_bb))

plt.plot(dom, preds_prom_bb,'r.', alpha=0.85, label=r'Preds')
plt.errorbar(dom, preds_prom_bb, preds_std_bb, fmt='none', ecolor='red')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.xticks(ticks=dom, labels=p_r_list)
plt.legend(loc="upper right")
plt.xlabel(r'Stage $[p_A, p_B]$')
plt.ylabel(r'Proporción')
plt.title(r"Preds vs Stage. $N=%i$ (%i rep)"%(n, total))
plt.grid()
#plt.savefig('BIC_vs_Stage_bayes_RLas_N%i_%i_esc(%i rep)'%(n, np_r, total), dpi=400)
#%% genera bayes, predice R-W asimétrico

arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-decisiones/Datos_simulados/Preds_bayes_RLas_N26_8_esc(100 rep).npz')

dataprom = arrays['arr_0']
preds = arrays['arr_1']
resultados = arrays['arr_2']

preds_prom_br = preds.mean(axis=0)
preds_std_br = preds.std(axis=0, ddof=1)/np.sqrt(total)

# Escenarios de las simuaciones
p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7], [.2, .8]]

np_r = 8#len(p_r_list)

n = 26 # número de trials

total = 100 # repeticiones por escenario para promediar

figsize(10, 8)

#BICs en función del escenario

plt.figure()

ax = plt.subplot(111)

dom = np.arange(0,len(preds_prom_br))


plt.plot(dom, preds_prom_br,'r.', alpha=0.85, label=r'Preds B/RW')
#plt.plot(dom, preds_prom_bb,'b.', alpha=0.85, label=r'Preds B/B')
plt.errorbar(dom, preds_prom_br, preds_std_br, fmt='none', ecolor='red')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.xticks(ticks=dom, labels=p_r_list)
plt.legend(loc="upper right")
plt.xlabel(r'Stage $[p_A, p_B]$')
plt.ylabel(r'Frecuencia promedio')
plt.title(r"Preds vs Stage. $N=%i$ (%i rep)"%(n, total))
plt.grid()
#%% Comparo Preds

dom = dom[:4]
preds_prom_br = preds_prom_br[:4]
preds_std_br = preds_std_br[:4]
preds_prom_bb = preds_prom_bb[:4]
preds_std_bb = preds_std_bb[:4]

plt.plot(dom, preds_prom_br,'r.', alpha=0.85, ms=10, label=r'Preds B/RW')
plt.errorbar(dom, preds_prom_br, preds_std_br, fmt='none', ecolor='red')
plt.plot(dom, preds_prom_rr,'b.', alpha=0.85, ms=10, label=r'Preds RW/RW')
plt.errorbar(dom, preds_prom_rr, preds_std_rr, fmt='none', ecolor='blue')
plt.plot(dom, preds_prom_rb,'g.', alpha=0.85, label=r'Preds RW/B')
plt.errorbar(dom, preds_prom_rb, preds_std_rb, fmt='none', ecolor='green')
plt.plot(dom, preds_prom_bb,'k.', alpha=0.85, label=r'Preds B/B')
plt.errorbar(dom, preds_prom_bb, preds_std_bb, fmt='none', ecolor='black')
plt.xticks(ticks=dom, labels=p_r_list)
plt.legend(loc="upper right")
plt.xlabel(r'Stage $[p_A, p_B]$')
plt.ylabel(r'Frecuencia promedio')
plt.title(r"Preds vs Stage. $N=%i$ (%i rep)"%(n, total))
plt.grid()

plt.savefig('Comp_Preds_RLas_bayes_N26_4_esc(100 rep).png', dpi=400)
#%% Imprimo en forma de matrices

from tabulate import tabulate


# for i in range(4):
#     dic = {'bayes':['bayes',preds_prom_bb[i], preds_prom_br[i]], 'RL':['RL',preds_prom_rb[i], preds_prom_rr[i]] }
    
#     print(tabulate(dic))

for i in range(4):
    norm = np.sum([preds_prom_bb[i], preds_prom_br[i], preds_prom_rb[i], preds_prom_rr[i]])
    dic = {'bayes':['bayes',preds_prom_bb[i]/norm, preds_prom_br[i]/norm], 'RL':['RL',preds_prom_rb[i]/norm, preds_prom_rr[i]/norm] }
    
    print(tabulate(dic))

