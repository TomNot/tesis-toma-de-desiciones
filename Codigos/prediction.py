#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 18:53:40 2021

@author: Tomas
"""


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 18:30:16 2021

@author: Tomas
"""
from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm, tnrange
import seaborn as sns
import theano
import arviz as az
import copy 
import arviz as az
from scipy.integrate import quad

#%%
#x es el rango de las betas
# x=np.arange(start=1/50,stop=49/50,step=1/50) 
    

## Bayesian inference over parameters Thompson sampling
    
# Defino una función que plotea los resultados de la simulación

def plot_data(actions, rewards, Qs):
    plt.figure(figsize=(20,3))
    x = np.arange(len(actions))

    plt.plot(x, Qs[:,0] - .5 + 0, c='C0', lw=3, alpha=.3)
    plt.plot(x, Qs[:,1] - .5 + 1, c='C1', lw=3, alpha=.3)

    s = 50
    lw = 2

    cond = (actions == 0) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C0', lw=lw)

    cond = (actions == 0) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C0', ec='C0', lw=lw)

    cond = (actions == 1) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C1', lw=lw)

    cond = (actions == 1) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C1', ec='C1', lw=lw)

    plt.scatter(0, 20, c='k', s=s, lw=lw, label='Reward')
    plt.scatter(0, 20, c='w', ec='k', s=s, lw=lw, label='No reward')
    plt.plot([0,1], [20, 20], c='k', lw=3, alpha=.3, label='b_value (centered)')


    plt.yticks([0,1], ['left', 'right'])
    plt.ylim(-1, 2)

    plt.ylabel('action')
    plt.xlabel('trial')

    handles, labels = plt.gca().get_legend_handles_labels()
    order = (1,2,0)
    handles = [handles[idx] for idx in order]
    labels = [labels[idx] for idx in order]

    plt.legend(handles, labels, fontsize=12, loc=(1.01, .27))
    plt.tight_layout()

# Genero data bayesiana
    
def generate_data_bayes(priorA, priorB, n=100, p_r=[.4, .6]):
    # paso de b,w a e,f
    expA = np.array([priorA[0]*priorA[1], priorA[1]-priorA[0]*priorA[1]]) #[e, f]
    expB = np.array([priorB[0]*priorB[1], priorB[1]-priorB[0]*priorB[1]]) #[e, f]
    
    #experiencia verdadera (sin prior)
    expAr = np.array([0, 0]) #[e, f]
    expBr = np.array([0, 0]) #[e, f]
    
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    
    Ps = np.zeros((n, 2))
    
    for i in range(n):
        # print(i)
        Ps[i] = np.array([expA[0]/np.sum(expA), expB[0]/np.sum(expB)])
        # Simulate choice and reward
        
        #eleccion de maquina 0 o 1
        if stats.beta.rvs(*expA,1)>stats.beta.rvs(*expB,1):
            eleccion=0
        else:
            eleccion=1    
        resultado = np.random.rand() < p_r[eleccion]
        
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            expA[0]=expA[0]+1
            expAr[0]=expAr[0]+1
        elif eleccion==0 and resultado==0:
            expA[1]=expA[1]+1
            expAr[1]=expAr[1]+1
        elif eleccion==1 and resultado==1:
            expB[0]=expB[0]+1
            expBr[0]=expBr[0]+1
        elif eleccion==1 and resultado==0:
            expB[1]=expB[1]+1
            expBr[1]=expBr[1]+1


        # Store values
        actions[i] = eleccion
        rewards[i] = resultado

    return actions, rewards, Ps, expA, expB, expAr, expBr


#largo del bloque bandit
n = 100

#probabilidades de pago verdaderas
p_r=[.4, .5]

#prior del agente thompson sampling
prior = [0.5, 1] #[b, w]
priorA = prior #[b, w]
priorB = prior #[b, w]


actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(priorA, priorB, n=n, p_r=p_r)

bs = np.array([expA[0]/np.sum(expA), expB[0]/np.sum(expB)])

plot_data(actions, rewards, Ps)
#%% defino las funciones para el modelo RW asimétrico

def update_Q(action, reward,
             Qs,
             alphas):
    """
    This function updates the Q table according to the RL update rule.
    It will be called by tt.scan to do so recursevely, given the observed data and the alpha parameter
    This could have been nlaced be the following lamba expression in the tt.scan fn argument:
        fn=lamba action, reward, Qs, alpha: tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    """
    Qs = tt.set_subtensor(Qs[action], Qs[action] + alphas[reward] * (reward - Qs[action]))
    return Qs

# Transform the variables into appropriate Theano objects
def theano_llik_td(alphas, beta, actions, rewards):
    rewards = theano.shared(np.asarray(rewards, dtype='int16'))
    actions = theano.shared(np.asarray(actions, dtype='int16'))

    # Compute the Qs values
    Qs = 0.5 * tt.ones((2), dtype='float64')
    Qs, updates = theano.scan(
        fn=update_Q,
        sequences=[actions, rewards],
        outputs_info=[Qs],
        non_sequences=[alphas])#[var]

    # Apply the sotfmax transformation
    Qs_ = Qs[:-1] * beta
    log_prob_actions = Qs_ - pm.math.logsumexp(Qs_, axis=1)

    # Calculate the negative log likelihod of the observed actions
    log_prob_actions = log_prob_actions[tt.arange(actions.shape[0]-1), actions[1:]]
    return tt.sum(log_prob_actions)  # PyMC makes it negative by default
#%% Calculo el BIC
def softmax(beta, Q, action):
    return np.exp(beta * Q[action])/np.sum(np.exp(beta*Q))

def choice(parms, actions, rewards, n):
    
    Q = np.ones((2)) * 0.5
        
    for i in range(n):
        action = actions[i]
        reward = rewards[i]
          
        Q[action] += parms[reward + 1] * (reward - Q[action])
        
        if i == (n-1):
            
            pprobA = softmax(parms[0], Q, 0)
                
    if pprobA > np.random.rand():
        choice=0
    else:
        choice=1
    print("\nchoice = %i"%choice)  
    return Q, choice

# def BIC(llik, npar, n):
#     return -2 * llik + npar * np.log(n)

def predict(actions, rewards):
    with pm.Model() as m:
        
        alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([true_alpha_f, true_alpha_s])) #empiezo de una uniforme
        beta = pm.Normal('beta', mu=5, sigma=.01, testval=5)
        
        # pm.Potential me permite definir "distribuciones" arbitrarias
        
        like = pm.Potential('like', theano_llik_td(alphas, beta, actions[:-1], rewards[:-1]))
        
        step = pm.Metropolis()
        trace = pm.sample(20000, step=step)
        burned_trace=trace[1000:]
        
    par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
    
    parms = par_fit[:,0]
     
    Q, action = choice(parms, actions, rewards, n)
    
    print("\naction = %i"%action) 
    
    print("predict = %i"%int(action==actions[-1]))
    
    print("predict final action = %i"%actions[-1])
    
    print(action==actions[-1])
    return int(action==actions[-1])
    

# true_alpha_s = .3
# true_alpha_f = .1

# n = 26
# npar = 3

# trials = np.arange(1,n+1)

# n = len(trials)

# total = 100

# #probabilidades de pago verdaderas

# p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7]]

# np_r = 8# len(p_r_list) # número de escenarios - 1

# datatot = np.zeros((npar,2,total,np_r))
# res = np.zeros((n,2,total,np_r))
# preds = np.zeros((total, np_r))

# j = -1
# for p_r in tqdm(p_r_list[:np_r], desc='Loop principal'):        
#     j += 1
    
#     dataprom = np.zeros((npar,2,total))
    
#     resultados = np.zeros((n,2,total))
    
#     for k in tqdm(range(total), desc='Loop promedio'):
                
#         actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(priorA, priorB, n=n, p_r=p_r)
        
#         print("\nfinal action = %i"%actions[-1])
        
#         resultados[:,:,k] = np.array([actions, rewards]).T
                   
#         with pm.Model() as m:
#             # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
#             # beta = pm.Exponential('beta', 10, testval=5)
            
#             # funciona mejor con estos priors que con los anteriores
            
#             alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([.1, .3])) #empiezo de una uniforme
#             beta = pm.Normal('beta', mu=5, sigma=.01, testval=5)
            
            
#             # pm.Potential me permite definir "distribuciones" arbitrarias
            
#             like = pm.Potential('like', theano_llik_td(alphas, beta, actions, rewards))
        
#             # trace = pm.sample() # sampleo por defecto
#             # burned_trace=trace[1000:]
            
#             # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
#                                                                 # era para arreglar un error
            
#             step = pm.Metropolis()
#             trace = pm.sample(20000, step=step)
#             burned_trace=trace[1000:]
            
#         par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
#         print(par_fit)
                
#         preds[k,j] += predict(actions,rewards)
        
#         dataprom[:,:,k] = par_fit
    
#     res[:,:,:,j] = resultados
#     datatot[:,:,:,j] = dataprom
    
    
# np.savez('Preds_bayes_RLas_N%i_%i_esc(%i rep).npz'%(n, np_r, total), datatot, preds, resultados)
# #%%

# n = 26

# total = 100

# #probabilidades de pago verdaderas

# p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7]]

# arrays = np.load('Preds_bayes_RLas_N26_2_esc(5 rep).npz')

# preds = arrays['arr_1']

# dfpreds = pd.DataFrame(preds)
# #%%
# plt.figure()

# plt.title('Prediction criterion vs escenarios $N=%i$, $rep=%i$'%(n,total))
# g = sns.catplot(x="time", y="pulse", hue="kind",
#                data=exercise, kind="violin")

#%% Simulo: genera bayes ajusta bayes


def integrandB(x, eA, fA, eB, fB):
    return stats.beta.pdf(x, eA+1, fA+1)*(1-stats.beta.cdf(x, eB+1, fB+1))

def confidence(parA, parB):
    # print(parA)
    # print(parB)
    
    IA = quad(integrandB, 0, 1, args=(*parB, *parA))[0]
    IB = quad(integrandB, 0, 1, args=(*parA, *parB))[0]
    
    # print(IA, IB)
    
    N = np.sum([IA, IB])
    
    res = [IA/N, IB/N]
    
    return np.array(res)

def choice_bayes(parms, actions, rewards, n):
    
    #experiencia verdadera (sin prior)
    s0, f0, s1, f1 = np.zeros((4))
    
    for l in tqdm(range(n-1), desc='Loop trials'):
        # print(l)
        eleccion = actions[l]
        resultado = rewards[l]
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        
        if eleccion==0 and resultado==1:
            s0=s0+1
        elif eleccion==0 and resultado==0:
            f0=f0+1
        elif eleccion==1 and resultado==1:
            s1=s1+1
        elif eleccion==1 and resultado==0:
            f1=f1+1
               
        
    b_fit = parms[0]
    w_fit = parms[1]
    
    # print('b='+str(b_fit)+'\nw='+str(w_fit))
      
    s0_p = b_fit*w_fit
    f0_p = (w_fit-b_fit*w_fit)
    
    s1_p = b_fit*w_fit
    f1_p = (w_fit-b_fit*w_fit)
    
    # print(s0_p+s0_total,f0_p+f0)
    # print(s1_p+s1,f1_p+f1)
    # print(l, b_fit)
    
    s0_real = s0_p + s0
    f0_real = f0_p + f0
    s1_real = s1_p + s1
    f1_real = f1_p + f1
                  
    if stats.beta.rvs(a=s0_real+1, b=f0_real+1,size=1)[0] > stats.beta.rvs(a=s1_real+1, b=f1_real+1,size=1)[0]:
        choice=0
    else:
        choice=1
    print("\nchoice = %i"%choice)  
    return choice
    

# n = 26
# npar = 3

# trials = np.arange(1,n+1)

# total = 100

# x=np.arange(start=1/50,stop=49/50,step=1/50) 

# ws=np.linspace(4.99, 5.01, len(x))

# #probabilidades de pago verdaderas

# p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7]]

# np_r = 4 #len(p_r_list) # número de escenarios - 1

# bpreds = np.zeros((total, np_r))

# datatot = np.zeros((npar,2,total,np_r))

# res = np.zeros((n,2,total,np_r))
        
# j = -1
# for p_r in tqdm(p_r_list[:np_r], desc='Loop principal'):        
#     j += 1
    
#     dataprom = np.zeros((npar,2,total))
    
#     resultados = np.zeros((n,2,total))
    
#     for k in tqdm(range(total), desc='Loop promedio'):   
        
#         actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(priorA, priorB, n=n, p_r=p_r)
            
#           #inicializo las listas
#         data = np.array((actions, rewards)).T
        
#         resultados[:,:,k] = data
    
#         p_prob = np.zeros((len(x), n))
#         prob = np.zeros((len(x), n))
        
#         #experiencia verdadera (sin prior)
#         s0, f0, s1, f1 = np.zeros((4))
        
#         for l in tqdm(range(n), desc='Loop trials'):
#             # print(l)
#             eleccion = actions[l]
#             resultado = rewards[l]
#             #actializo parametros de Beta posterior (usada para comportamiento)
#             #y actualizo la experiencia real (usada luego para el fit)
            
#             if eleccion==0 and resultado==1:
#                 s0=s0+1
#             elif eleccion==0 and resultado==0:
#                 f0=f0+1
#             elif eleccion==1 and resultado==1:
#                 s1=s1+1
#             elif eleccion==1 and resultado==0:
#                 f1=f1+1
                   
#             i_0=-1
#             #llamo b_fit al unico parametros libre (asumo b0=b1 y w verdadero)
#             for b_fit, w_fit in zip(x, ws):
#                 i_0=i_0+1
                
#                 # print('b='+str(b_fit)+'\nw='+str(w_fit))
                  
#                 s0_p = b_fit*w_fit
#                 f0_p = (w_fit-b_fit*w_fit)
                
#                 s1_p = b_fit*w_fit
#                 f1_p = (w_fit-b_fit*w_fit)
                
#                 # print(s0_p+s0_total,f0_p+f0)
#                 # print(s1_p+s1,f1_p+f1)
#                 # print(l, b_fit)
                
#                 s0_real = s0_p + s0
#                 f0_real = f0_p + f0
#                 s1_real = s1_p + s1
#                 f1_real = f1_p + f1
                
#                 #computo pdf's y cdf's
#                 prob0, prob1 = confidence([s0_real, f0_real], [s1_real,f1_real])
                
#                 # print('prob0='+str(prob0))
#                 # print('prob1='+str(prob1))
                
#                 #probabilidad de la eleccion dado el prior (labeled by i_0)
#                 if eleccion==0:
#                     p_prob[i_0,l]=np.log(prob0)
#                 elif eleccion==1:
#                     p_prob[i_0,l]=np.log(prob1)
               
#                 #i.i.d. + np.log sapce
#                 prob[i_0,l]=np.sum(p_prob[i_0,1:l])
                   
#             #normalizo y paso a linear space
                        
#             prob[:,l]=np.exp(prob[:,l])/np.sum(np.exp(prob[:,l]))
        
#         # plt.plot(x, prob[:,-1], label='Rep = %i'%k)
#         # # Plot formatting
#         # plt.legend(prop={'size': 6})
#         # plt.title(r'Density Plot $b$')
#         # plt.xlabel(r'$b$ value')
#         # plt.ylabel('Density')
         
#         b_ok = np.average(x, weights=prob[:,-2])
#         w_ok = np.average(ws, weights=prob[:,-2])
        
#         action = choice_bayes([b_ok, w_ok], actions, rewards, n)
        
#         bpreds[k,j] += int(action==actions[-1])
        
#         dataprom[0,:,k] = [np.average(x, weights=prob[:,-1]), np.sqrt(np.cov(x, aweights=prob[:,-1]))]
#         dataprom[1,:,k] = [np.average(ws, weights=prob[:,-1]), np.sqrt(np.cov(ws, aweights=prob[:,-1]))]
    
#     res[:,:,:,j] = resultados
#     datatot[:,:,:,j] = dataprom

# print(bpreds)

# np.savez('Preds_bayes_bayes_N%i_%i_esc(%i rep).npz'%(n, np_r, total), datatot, bpreds, res)
#%% Simulo: genera RWas ajusta RWas


def generate_data_RLas(alpha_s, alpha_f, beta, n=100, p_r=[.4, .6]):
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    Qs = np.zeros((n, 2))

    # Initialize Q table
    Q = np.array([.5, .5])
    for i in range(n):
        # Apply the Softmax transformation
        exp_Q = np.exp(beta*Q)
        prob_a = exp_Q / np.sum(exp_Q)

        # Simulate choice and reward
        a = np.random.choice([0, 1], p=prob_a)
        r = np.random.rand() < p_r[a]

        # Update Q table
        if r:
            Q[a] = Q[a] + alpha_s * (r - Q[a])
            # print('r=1')
        else:
            Q[a] = Q[a] + alpha_f * (r - Q[a])
            # print('r=0')

        # Store values
        actions[i] = a
        rewards[i] = r
        Qs[i] = copy.deepcopy(Q) # copio para no sobreescribir la variable

    return actions, rewards, Qs

true_alpha_s = .3
true_alpha_f = .1
true_beta = 5

# n = 26
# npar = 3

# trials = np.arange(1,n+1)

# n = len(trials)

# total = 100

# #probabilidades de pago verdaderas

# p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7]]

# np_r = 4# len(p_r_list) # número de escenarios - 1

# datatot = np.zeros((npar,2,total,np_r))
# res = np.zeros((n,2,total,np_r))
# preds = np.zeros((total, np_r))

# j = -1
# for p_r in tqdm(p_r_list[:np_r], desc='Loop principal'):        
#     j += 1
    
#     dataprom = np.zeros((npar,2,total))
    
#     resultados = np.zeros((n,2,total))
    
#     for k in tqdm(range(total), desc='Loop promedio'):
                
#         actions, rewards, Qs = generate_data_RLas(true_alpha_s, true_alpha_f, true_beta, n, p_r=p_r)
        
#         print("\nfinal action = %i"%actions[-1])
        
#         resultados[:,:,k] = np.array([actions, rewards]).T
                   
#         with pm.Model() as m:
#             # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
#             # beta = pm.Exponential('beta', 10, testval=5)
            
#             # funciona mejor con estos priors que con los anteriores
            
#             alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([.1, .3])) #empiezo de una uniforme
#             beta = pm.Normal('beta', mu=5, sigma=.01, testval=5)
            
            
#             # pm.Potential me permite definir "distribuciones" arbitrarias
            
#             like = pm.Potential('like', theano_llik_td(alphas, beta, actions, rewards))
        
#             # trace = pm.sample() # sampleo por defecto
#             # burned_trace=trace[1000:]
            
#             # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
#                                                                 # era para arreglar un error
            
#             step = pm.Metropolis()
#             trace = pm.sample(20000, step=step)
#             burned_trace=trace[1000:]
            
#         par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
#         print(par_fit)
                
#         preds[k,j] += predict(actions,rewards)
        
#         dataprom[:,:,k] = par_fit
    
#     res[:,:,:,j] = resultados
#     datatot[:,:,:,j] = dataprom
    
    
# np.savez('Preds_RLas_RLas_N%i_%i_esc(%i rep).npz'%(n, np_r, total), datatot, preds, resultados)
#%% Simulo: genera RWas ajusta bayes

n = 26
npar = 3

trials = np.arange(1,n+1)

total = 100

x=np.arange(start=1/50,stop=49/50,step=1/50) 

ws=np.linspace(4.99, 5.01, len(x))

#probabilidades de pago verdaderas

p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7]]

np_r = 4 #len(p_r_list) # número de escenarios - 1

bpreds = np.zeros((total, np_r))

datatot = np.zeros((npar,2,total,np_r))

res = np.zeros((n,2,total,np_r))
        
j = -1
for p_r in tqdm(p_r_list[:np_r], desc='Loop principal'):        
    j += 1
    
    dataprom = np.zeros((npar,2,total))
    
    resultados = np.zeros((n,2,total))
    
    for k in tqdm(range(total), desc='Loop promedio'):   
        
        actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(priorA, priorB, n=n, p_r=p_r)
            
          #inicializo las listas
        data = np.array((actions, rewards)).T
        
        resultados[:,:,k] = data
    
        p_prob = np.zeros((len(x), n))
        prob = np.zeros((len(x), n))
        
        #experiencia verdadera (sin prior)
        s0, f0, s1, f1 = np.zeros((4))
        
        for l in tqdm(range(n), desc='Loop trials'):
            # print(l)
            eleccion = actions[l]
            resultado = rewards[l]
            #actializo parametros de Beta posterior (usada para comportamiento)
            #y actualizo la experiencia real (usada luego para el fit)
            
            if eleccion==0 and resultado==1:
                s0=s0+1
            elif eleccion==0 and resultado==0:
                f0=f0+1
            elif eleccion==1 and resultado==1:
                s1=s1+1
            elif eleccion==1 and resultado==0:
                f1=f1+1
                   
            i_0=-1
            #llamo b_fit al unico parametros libre (asumo b0=b1 y w verdadero)
            for b_fit, w_fit in zip(x, ws):
                i_0=i_0+1
                
                # print('b='+str(b_fit)+'\nw='+str(w_fit))
                  
                s0_p = b_fit*w_fit
                f0_p = (w_fit-b_fit*w_fit)
                
                s1_p = b_fit*w_fit
                f1_p = (w_fit-b_fit*w_fit)
                
                # print(s0_p+s0_total,f0_p+f0)
                # print(s1_p+s1,f1_p+f1)
                # print(l, b_fit)
                
                s0_real = s0_p + s0
                f0_real = f0_p + f0
                s1_real = s1_p + s1
                f1_real = f1_p + f1
                
                #computo pdf's y cdf's
                prob0, prob1 = confidence([s0_real, f0_real], [s1_real,f1_real])
                
                # print('prob0='+str(prob0))
                # print('prob1='+str(prob1))
                
                #probabilidad de la eleccion dado el prior (labeled by i_0)
                if eleccion==0:
                    p_prob[i_0,l]=np.log(prob0)
                elif eleccion==1:
                    p_prob[i_0,l]=np.log(prob1)
               
                #i.i.d. + np.log sapce
                prob[i_0,l]=np.sum(p_prob[i_0,1:l])
                   
            #normalizo y paso a linear space
                        
            prob[:,l]=np.exp(prob[:,l])/np.sum(np.exp(prob[:,l]))
        
        # plt.plot(x, prob[:,-1], label='Rep = %i'%k)
        # # Plot formatting
        # plt.legend(prop={'size': 6})
        # plt.title(r'Density Plot $b$')
        # plt.xlabel(r'$b$ value')
        # plt.ylabel('Density')
         
        b_ok = np.average(x, weights=prob[:,-2])
        w_ok = np.average(ws, weights=prob[:,-2])
        
        action = choice_bayes([b_ok, w_ok], actions, rewards, n)
        
        bpreds[k,j] += int(action==actions[-1])
        
        dataprom[0,:,k] = [np.average(x, weights=prob[:,-1]), np.sqrt(np.cov(x, aweights=prob[:,-1]))]
        dataprom[1,:,k] = [np.average(ws, weights=prob[:,-1]), np.sqrt(np.cov(ws, aweights=prob[:,-1]))]
    
    res[:,:,:,j] = resultados
    datatot[:,:,:,j] = dataprom

print(bpreds)

np.savez('Preds_RLas_bayes_N%i_%i_esc(%i rep).npz'%(n, np_r, total), datatot, bpreds, res)