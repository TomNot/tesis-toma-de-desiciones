#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 18:30:16 2021

@author: Tomas
"""
from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm, tnrange
import seaborn as sns
import theano
import arviz as az
import copy 
import arviz as az
from scipy.integrate import quad

#%%
#x es el rango de las betas
# x=np.arange(start=1/50,stop=49/50,step=1/50) 
    

## Bayesian inference over parameters Thompson sampling
    
# Defino una función que plotea los resultados de la simulación

def plot_data(actions, rewards, Qs):
    plt.figure(figsize=(20,3))
    x = np.arange(len(actions))

    plt.plot(x, Qs[:,0] - .5 + 0, c='C0', lw=3, alpha=.3)
    plt.plot(x, Qs[:,1] - .5 + 1, c='C1', lw=3, alpha=.3)

    s = 50
    lw = 2

    cond = (actions == 0) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C0', lw=lw)

    cond = (actions == 0) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C0', ec='C0', lw=lw)

    cond = (actions == 1) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C1', lw=lw)

    cond = (actions == 1) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C1', ec='C1', lw=lw)

    plt.scatter(0, 20, c='k', s=s, lw=lw, label='Reward')
    plt.scatter(0, 20, c='w', ec='k', s=s, lw=lw, label='No reward')
    plt.plot([0,1], [20, 20], c='k', lw=3, alpha=.3, label='b_value (centered)')


    plt.yticks([0,1], ['left', 'right'])
    plt.ylim(-1, 2)

    plt.ylabel('action')
    plt.xlabel('trial')

    handles, labels = plt.gca().get_legend_handles_labels()
    order = (1,2,0)
    handles = [handles[idx] for idx in order]
    labels = [labels[idx] for idx in order]

    plt.legend(handles, labels, fontsize=12, loc=(1.01, .27))
    plt.tight_layout()

# Genero data bayesiana
    
def generate_data_bayes(priorA, priorB, n=100, p_r=[.4, .6]):
    # paso de b,w a e,f
    expA = np.array([priorA[0]*priorA[1], priorA[1]-priorA[0]*priorA[1]]) #[e, f]
    expB = np.array([priorB[0]*priorB[1], priorB[1]-priorB[0]*priorB[1]]) #[e, f]
    
    #experiencia verdadera (sin prior)
    expAr = np.array([0, 0]) #[e, f]
    expBr = np.array([0, 0]) #[e, f]
    
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    
    Ps = np.zeros((n, 2))
    
    for i in range(n):
        # print(i)
        Ps[i] = np.array([expA[0]/np.sum(expA), expB[0]/np.sum(expB)])
        # Simulate choice and reward
        
        #eleccion de maquina 0 o 1
        if stats.beta.rvs(*expA,1)>stats.beta.rvs(*expB,1):
            eleccion=0
        else:
            eleccion=1    
        resultado = np.random.rand() < p_r[eleccion]
        
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            expA[0]=expA[0]+1
            expAr[0]=expAr[0]+1
        elif eleccion==0 and resultado==0:
            expA[1]=expA[1]+1
            expAr[1]=expAr[1]+1
        elif eleccion==1 and resultado==1:
            expB[0]=expB[0]+1
            expBr[0]=expBr[0]+1
        elif eleccion==1 and resultado==0:
            expB[1]=expB[1]+1
            expBr[1]=expBr[1]+1


        # Store values
        actions[i] = eleccion
        rewards[i] = resultado

    return actions, rewards, Ps, expA, expB, expAr, expBr


#largo del bloque bandit
n = 100

#probabilidades de pago verdaderas
p_r=[.4, .5]

#prior del agente thompson sampling
prior = [0.3, 5] #[b, w]
priorA = prior #[b, w]
priorB = prior #[b, w]


actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(priorA, priorB, n=n, p_r=p_r)

bs = np.array([expA[0]/np.sum(expA), expB[0]/np.sum(expB)])

plot_data(actions, rewards, Ps)
#%% defino las funciones para el modelo RW asimétrico

def update_Q(action, reward,
             Qs,
             alphas):
    """
    This function updates the Q table according to the RL update rule.
    It will be called by tt.scan to do so recursevely, given the observed data and the alpha parameter
    This could have been nlaced be the following lamba expression in the tt.scan fn argument:
        fn=lamba action, reward, Qs, alpha: tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    """
    Qs = tt.set_subtensor(Qs[action], Qs[action] + alphas[reward] * (reward - Qs[action]))
    return Qs

# Transform the variables into appropriate Theano objects
def theano_llik_td(alphas, beta, actions, rewards):
    rewards = theano.shared(np.asarray(rewards, dtype='int16'))
    actions = theano.shared(np.asarray(actions, dtype='int16'))

    # Compute the Qs values
    Qs = 0.5 * tt.ones((2), dtype='float64')
    Qs, updates = theano.scan(
        fn=update_Q,
        sequences=[actions, rewards],
        outputs_info=[Qs],
        non_sequences=[alphas])#[var]

    # Apply the sotfmax transformation
    Qs_ = Qs[:-1] * beta
    log_prob_actions = Qs_ - pm.math.logsumexp(Qs_, axis=1)

    # Calculate the negative log likelihod of the observed actions
    log_prob_actions = log_prob_actions[tt.arange(actions.shape[0]-1), actions[1:]]
    return tt.sum(log_prob_actions)  # PyMC makes it negative by default
#%% Simulo: genera bayes ajusta RWas
    
n = 1000
npar = 3

trials = np.arange(1,n+1)

total = 1

#probabilidades de pago verdaderas

# p_r_list=[[.1, .2], [.1, .9], [.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7], [.2, .8]]
p_r_list = [[.2,.8]]
np_r = len(p_r_list) # número de escenarios - 1

datatot = np.zeros((npar,2,n,total,np_r))
res = np.zeros((n,2,total,np_r))
alphas_t = np.zeros((2,2,total,np_r))

j = -1
for p_r in tqdm(p_r_list[:np_r], desc='Loop principal'):        
    j += 1
    
    dataprom = np.zeros((npar,2,n,total))
    
    resultados = np.zeros((n,2,total))
    
    for k in tqdm(range(total), desc='Loop promedio'):
        data = np.zeros((npar,2,n))
                
        actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(priorA, priorB, n=n, p_r=p_r)
        
        resultados[:,:,k] = np.array([actions, rewards]).T
        
        # Cuenta simple que hicimos con Pablo relacionando RWas con bayesiano 
        # No tiene en cuenta el sampleo de Thompon
        df = pd.DataFrame(list(zip(actions, rewards)), columns=['Actions', 'Rewards'], index=['%i'%(i+1) for i in range(n)])
        
        dfA = df[df['Actions']==0]
        dfB = df[df['Actions']==1]
        
        wA = dfA['Actions'].count()
        wB = dfB['Actions'].count()
        
        eA = dfA['Rewards'].sum()
        eB = dfB['Rewards'].sum()
        
        fA = wA - eA
        fB = wB - eB
        
        alpha_masA = ( (eA+1)/(n+ 1) - eA/n) / (1 - eA/n)
        alpha_masB = ( (eB+1)/(n+ 1) - eB/n) / (1 - eB/n)
        
        alpha_menosA = ( (eA)/(n+ 1) - eA/n) / (0 - eA/n)
        alpha_menosB = ( (eB)/(n+ 1) - eB/n) / (0 - eB/n)
        
        alphas_t[:,0,k,j] = [alpha_menosA, alpha_masA]
        alphas_t[:,1,k,j] = [alpha_menosB, alpha_masB]
        i=-1
        for h_trial in tqdm(trials, desc='Loop trials'):
            i+=1
            
            with pm.Model() as m:
                # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
                # beta = pm.Exponential('beta', 10, testval=5)
                
                # funciona mejor con estos priors que con los anteriores
                
                alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([.1, .3])) #empiezo de una uniforme
                beta = pm.Normal('beta', mu=5, sigma=.01, testval=5)
                
                
                # pm.Potential me permite definir "distribuciones" arbitrarias
                
                like = pm.Potential('like', theano_llik_td(alphas, beta, actions[:h_trial], rewards[:h_trial]))
            
                # trace = pm.sample() # sampleo por defecto
                # burned_trace=trace[1000:]
                
                # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                                    # era para arreglar un error
                
                step = pm.Metropolis()
                trace = pm.sample(20000, step=step)
                burned_trace=trace[1000:]
                
            par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
            print(par_fit)
            data[:,:,i] = par_fit
            print(data[:,:,i])
        dataprom[:,:,:,k] = data
    res[:,:,:,j] = resultados
    datatot[:,:,:,:,j] = dataprom
    
    
np.savez('bayes_RLas_N%i_%i_esc(%i rep).npz'%(n, np_r, total), datatot, res, alphas_t)
