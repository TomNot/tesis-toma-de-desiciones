#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 27 12:59:42 2021

@author: Tomas
"""
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import scipy.stats as stats
from tqdm import tqdm 
import theano
import copy
#%%
#defino las funciones para el modelo RW asimétrico

def update_Q(action, reward,
             Qs,
             alphas):
    """
    This function updates the Q table according to the RL update rule.
    It will be called by tt.scan to do so recursevely, given the observed data and the alpha parameter
    This could have been nlaced be the following lamba expression in the tt.scan fn argument:
        fn=lamba action, reward, Qs, alpha: tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    """
    Qs = tt.set_subtensor(Qs[action], Qs[action] + alphas[reward] * (reward - Qs[action]))
    return Qs

# Transform the variables into appropriate Theano objects
def theano_llik_td(alphas, beta, actions, rewards):
    rewards = theano.shared(np.asarray(rewards, dtype='int16'))
    actions = theano.shared(np.asarray(actions, dtype='int16'))

    # Compute the Qs values
    Qs = 0.5 * tt.ones((2), dtype='float64')
    Qs, updates = theano.scan(
        fn=update_Q,
        sequences=[actions, rewards],
        outputs_info=[Qs],
        non_sequences=[alphas])#[var]

    # Apply the sotfmax transformation
    Qs_ = Qs[:-1] * beta
    log_prob_actions = Qs_ - pm.math.logsumexp(Qs_, axis=1)

    # Calculate the negative log likelihod of the observed actions
    log_prob_actions = log_prob_actions[tt.arange(actions.shape[0]-1), actions[1:]]
    return tt.sum(log_prob_actions)  # PyMC makes it negative by default

def generate_data_RLas(alpha_s, alpha_f, beta, n=100, p_r=[.4, .6]):
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    Qs = np.zeros((n, 2))

    # Initialize Q table
    Q = np.array([.5, .5])
    for i in range(n):
        # Apply the Softmax transformation
        exp_Q = np.exp(beta*Q)
        prob_a = exp_Q / np.sum(exp_Q)

        # Simulate choice and reward
        a = np.random.choice([0, 1], p=prob_a)
        r = np.random.rand() < p_r[a]

        # Update Q table
        if r:
            Q[a] = Q[a] + alpha_s * (r - Q[a])
            # print('r=1')
        else:
            Q[a] = Q[a] + alpha_f * (r - Q[a])
            # print('r=0')

        # Store values
        actions[i] = a
        rewards[i] = r
        Qs[i] = copy.deepcopy(Q) # copio para no sobreescribir la variable

    return actions, rewards, Qs

def generate_data_bayes(priorA, priorB, n=100, p_r=[.4, .6]):
    # paso de b,w a e,f
    expA = np.array([priorA[0]*priorA[1], priorA[1]-priorA[0]*priorA[1]]) #[e, f]
    expB = np.array([priorB[0]*priorB[1], priorB[1]-priorB[0]*priorB[1]]) #[e, f]
    
    #experiencia verdadera (sin prior)
    expAr = np.array([0, 0]) #[e, f]
    expBr = np.array([0, 0]) #[e, f]
    
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    
    Ps = np.zeros((n, 2))
    
    for i in range(n):
        # print(i)
        Ps[i] = np.array([(expA[0]+1)/(np.sum(expA)+2), (expB[0]+1)/(np.sum(expB)+2)])
        # Simulate choice and reward
        
        #eleccion de maquina 0 o 1
        if stats.beta.rvs(*expA,1)>stats.beta.rvs(*expB,1):
            eleccion=0
        else:
            eleccion=1    
        resultado = np.random.rand() < p_r[eleccion]
        
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            expA[0]=expA[0]+1
            expAr[0]=expAr[0]+1
        elif eleccion==0 and resultado==0:
            expA[1]=expA[1]+1
            expAr[1]=expAr[1]+1
        elif eleccion==1 and resultado==1:
            expB[0]=expB[0]+1
            expBr[0]=expBr[0]+1
        elif eleccion==1 and resultado==0:
            expB[1]=expB[1]+1
            expBr[1]=expBr[1]+1


        # Store values
        actions[i] = eleccion
        rewards[i] = resultado

    return actions, rewards, Ps, expA, expB, expAr, expBr

prior = [.6,50]
priorA, priorB = prior, prior

n = 1000
npar = 3

trials = np.arange(1,n+1,50)


total = 1

#probabilidades de pago verdaderas

p_r_list=[[.2, .6]]#, [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7], [.2, .8]]

np_r = 1#len(p_r_list) # número de escenarios - 1

datatot = np.zeros((npar,2,n,total,np_r))
res = np.zeros((n,2,total,np_r))

for j, p_r in tqdm(enumerate(p_r_list[:np_r]), desc='Loop principal'):        
    
    dataprom = np.zeros((npar,2,len(trials),total))
    
    resultados = np.zeros((n,2,total))
    
    for k in tqdm(range(total), desc='Loop promedio'):
        data = np.zeros((npar,2,len(trials)))
                
        actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(priorA, priorB, n=n, p_r=p_r)
        
        resultados[:,:,k] = np.array([actions, rewards]).T
             
        for i, h_trial in tqdm(enumerate(trials), desc='Loop trials'):
            
            with pm.Model() as m:
                # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
                # beta = pm.Exponential('beta', 10, testval=5)
                
                # funciona mejor con estos priors que con los anteriores
                    
                alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([.1, .3])) #empiezo de una uniforme
                beta = pm.Normal('beta', mu=5, sigma=.01, testval=5)
            
            
#             # pm.Potential me permite definir "distribuciones" arbitrarias
            
                like = pm.Potential('like', theano_llik_td(alphas, beta, actions[:h_trial], rewards[:h_trial]))
            
                # trace = pm.sample() # sampleo por defecto
                # burned_trace=trace[1000:]
                
                # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                                    # era para arreglar un error
                
                step = pm.Metropolis()
                trace = pm.sample(20000, step=step)
                burned_trace=trace[1000:]
                    
            par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
            print(par_fit)
            data[:,:,i] = par_fit
            print(data[:,:,i])
        dataprom[:,:,:,k] = data
    #res[:,:,:,:,j] = resultados
    #datatot[:,:,:,:,j] = dataprom
#np.savez('bayes_RL_N%i_%i_esc(%i rep).npz'%(n, np_r, total), datatot, res)
np.savez('bayes_RLas_N%i_%i_esc(%i rep)_.2, .6_w50b%.1f.npz'%(n, np_r, total, prior[0]), dataprom, resultados)
#%% Simulo: genera RWas ajusta RWas

true_alpha_s = .4
true_alpha_f = .7
true_beta = 5
    
# n = 500
npar = 3

# trials = np.arange(1,n+1)

total = 1

#probabilidades de pago verdaderas

#p_r_list=[[.8, .9], [.4, .5], [.4, .6], [.3, .5], [.2, .8], [.3, .7]]

np_r = 1#len(p_r_list) # número de escenarios - 1

datatot = np.zeros((npar,2,n,total,np_r))
res = np.zeros((n,2,total,np_r))

j = -1
for p_r in tqdm(p_r_list[:np_r], desc='Loop principal'):        
    j += 1
    
    dataprom = np.zeros((npar,2,n,total))
    
    resultados = np.zeros((n,2,total))
    
    for k in tqdm(range(total), desc='Loop promedio'):
        data = np.zeros((npar,2,n))
                
        actions, rewards, Qs = generate_data_RLas(true_alpha_s, true_alpha_f, true_beta, n, p_r=p_r)
        
        resultados[:,:,k] = np.array([actions, rewards]).T
        
        i=-1        
        for h_trial in tqdm(trials, desc='Loop trials'):
            i+=1
            
            with pm.Model() as m:
                # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
                # beta = pm.Exponential('beta', 10, testval=5)
                
                # funciona mejor con estos priors que con los anteriores
                
                alphas = pm.Beta('alphas', 1, 1, shape=2, testval=np.array([.1, .3])) #empiezo de una uniforme
                betap = pm.Normal('beta', mu=5, sigma=.01, testval=5)
                
                
                # pm.Potential me permite definir "distribuciones" arbitrarias
                
                like = pm.Potential('like', theano_llik_td(alphas, betap, actions[:h_trial], rewards[:h_trial]))
            
                # trace = pm.sample() # sampleo por defecto
                # burned_trace=trace[1000:]
                
                # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                                    # era para arreglar un error
                
                step = pm.Metropolis()
                trace = pm.sample(20000, step=step)
                burned_trace=trace[1000:]
                
            par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
            print(par_fit)
            data[:,:,i] = par_fit
            print(data[:,:,i])
        dataprom[:,:,:,k] = data
    #res[:,:,:,j] = resultados
    #datatot[:,:,:,:,j] = dataprom
    
np.savez('RLas_RLas_N%i_%i_esc(%i rep)_.2, .6_alfas.7.4.npz'%(n, np_r, total), dataprom, resultados)