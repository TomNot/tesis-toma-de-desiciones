#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Nov 10 19:25:37 2020

@author: Tomas
"""
from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm 
import seaborn as sns
import theano
import arviz as az
#%% Bayesiano
true_p_A = 0.9
true_p_B = 0.2

N = 100

N_A = 50 #cantidad de elecciones de A
N_B = N - N_A #cantidad de elecciones de B

#generate some observations
observations_A = stats.bernoulli.rvs(true_p_A, size=N_A)
observations_B = stats.bernoulli.rvs(true_p_B, size=N_B)

with pm.Model() as bayes:
    # Define parameters priors
    p_A = pm.Uniform("p_A", 0, 1, testval=0.5)
    p_B = pm.Uniform("p_B", 0, 1, testval=0.5)
    
    # Define the deterministic delta function. This is our unknown of interest.
    delta = pm.Deterministic("delta", p_A - p_B)

    
    # Set of observations, in this case we have two observation datasets.
    obs_A = pm.Bernoulli("obs_A", p_A, observed=observations_A)
    obs_B = pm.Bernoulli("obs_B", p_B, observed=observations_B)

    # Posterior sampling algorithm.
    step = pm.Metropolis()
    trace = pm.sample(20000, step=step)
    burned_trace=trace[1000:] # tomo el sampleo a partir del 1000 para 
                              # independizarme de la condición inicial
    
#%%
# trace nos da el sampling del posterior
p_A_samples = burned_trace["p_A"]
p_B_samples = burned_trace["p_B"]
delta_samples = burned_trace["delta"]
#%%
figsize(10, 8)

#histogram of posteriors

ax = plt.subplot(311)

plt.xlim(0, 1)
plt.hist(p_A_samples, histtype='stepfilled', bins=25, alpha=0.85,
         label="posterior of $p_A$", color="#A60628", normed=True)
plt.vlines(true_p_A, 0, 10, linestyle="--", label="true $p_A$ (unknown)")
plt.legend(loc="upper right")
plt.title("Posterior distributions of $p_A$, $p_B$, and delta unknowns")

ax = plt.subplot(312)

plt.xlim(0, 1)
plt.hist(p_B_samples, histtype='stepfilled', bins=25, alpha=0.85,
         label="posterior of $p_B$", color="#467821", normed=True)
plt.vlines(true_p_B, 0, 10, linestyle="--", label="true $p_B$ (unknown)")
plt.legend(loc="upper right")

ax = plt.subplot(313)
plt.hist(delta_samples, histtype='stepfilled', bins=30, alpha=0.85,
         label="posterior of delta", color="#7A68A6", normed=True)
plt.vlines(true_p_A - true_p_B, 0, 5, linestyle="--",
           label="true delta (unknown)")
# plt.vlines(0, 0, 5, color="black", alpha=0.2)
plt.legend(loc="upper right");
plt.tight_layout();

#%% Rescorla-Wagner

# Defino una función que plotea los resultados de la simulación

def plot_data(actions, rewards, Qs):
    plt.figure(figsize=(20,3))
    x = np.arange(len(actions))

    plt.plot(x, Qs[:, 0] - .5 + 0, c='C0', lw=3, alpha=.3)
    plt.plot(x, Qs[:, 1] - .5 + 1, c='C1', lw=3, alpha=.3)

    s = 50
    lw = 2

    cond = (actions == 0) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C0', lw=lw)

    cond = (actions == 0) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C0', ec='C0', lw=lw)

    cond = (actions == 1) & (rewards == 0)
    plt.scatter(x[cond], actions[cond], s=s, c='None', ec='C1', lw=lw)

    cond = (actions == 1) & (rewards == 1)
    plt.scatter(x[cond], actions[cond], s=s, c='C1', ec='C1', lw=lw)

    plt.scatter(0, 20, c='k', s=s, lw=lw, label='Reward')
    plt.scatter(0, 20, c='w', ec='k', s=s, lw=lw, label='No reward')
    plt.plot([0,1], [20, 20], c='k', lw=3, alpha=.3, label='Qvalue (centered)')


    plt.yticks([0,1], ['left', 'right'])
    plt.ylim(-1, 2)

    plt.ylabel('action')
    plt.xlabel('trial')

    handles, labels = plt.gca().get_legend_handles_labels()
    order = (1,2,0)
    handles = [handles[idx] for idx in order]
    labels = [labels[idx] for idx in order]

    plt.legend(handles, labels, fontsize=12, loc=(1.01, .27))
    plt.tight_layout()

# Genero data 
    
def generate_data(alpha, beta, n=100, p_r=[.4, .6]):
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    Qs = np.zeros((n, 2))

    # Initialize Q table
    Q = np.array([.5, .5])
    for i in range(n):
        # Apply the Softmax transformation
        exp_Q = np.exp(beta*Q)
        prob_a = exp_Q / np.sum(exp_Q)

        # Simulate choice and reward
        a = np.random.choice([0, 1], p=prob_a)
        r = np.random.rand() < p_r[a]

        # Update Q table
        Q[a] = Q[a] + alpha * (r - Q[a])

        # Store values
        actions[i] = a
        rewards[i] = r
        Qs[i] = Q.copy() # copio para no sobreescribir la variable

    return actions, rewards, Qs


true_alpha = .3
true_beta = 5

#número de trials
n = 100
#probabilidades de pago verdaderas
p_r=[.8, .9]

actions, rewards, Qs = generate_data(true_alpha, true_beta, n, p_r=p_r)

plot_data(actions, rewards, Qs)

#%%
def update_Q(action, reward,
             Qs,
             alpha):
    """
    This function updates the Q table according to the RL update rule.
    It will be called by tt.scan to do so recursevely, given the observed data and the alpha parameter
    This could have been replaced be the following lamba expression in the tt.scan fn argument:
        fn=lamba action, reward, Qs, alpha: tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    """

    Qs = tt.set_subtensor(Qs[action], Qs[action] + alpha * (reward - Qs[action]))
    return Qs

#%%
# Transform the variables into appropriate Theano objects
def theano_llik_td(alpha, beta, actions, rewards):
    rewards = theano.shared(np.asarray(rewards, dtype='int16'))
    actions = theano.shared(np.asarray(actions, dtype='int16'))

    # Compute the Qs values
    Qs = 0.5 * tt.ones((2), dtype='float64')
    Qs, updates = theano.scan(
        fn=update_Q,
        sequences=[actions, rewards],
        outputs_info=[Qs],
        non_sequences=[alpha])

    # Apply the sotfmax transformation
    Qs_ = Qs[:-1] * beta
    log_prob_actions = Qs_ - pm.math.logsumexp(Qs_, axis=1)

    # Calculate the negative log likelihod of the observed actions
    log_prob_actions = log_prob_actions[tt.arange(actions.shape[0]-1), actions[1:]]
    return tt.sum(log_prob_actions)  # PyMC makes it negative by default


#%%
with pm.Model() as m:
    # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
    # beta = pm.Exponential('beta', 10, testval=5)
    
    # funciona mejor con estos priors que con los anteriores
    
    alpha = pm.Beta('alpha', 1, 1, testval=0.3)
    beta = pm.HalfNormal('beta', 10, testval=5)
    
    # pm.Potential me permite definir "distribuciones" arbitrarias
    
    like = pm.Potential('like', theano_llik_td(alpha, beta, actions, rewards))

    # trace = pm.sample() # sampleo por defecto
    # burned_trace=trace[1000:]
    
    # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                        # era para arreglar un error
    
    step = pm.Metropolis()
    trace = pm.sample(20000, step=step)
    burned_trace=trace[1000:]

    
az.plot_posterior(burned_trace)
par_fit = pm.summary(trace).iloc[:,:2]
#%%
alphas = np.arange(0.01, 0.99, 0.05)
# betas = 5

# true_alpha = .3
true_beta = 5

n = 100


Rep = len(alphas)

data = np.zeros((2,2,Rep))

i=-1
for true_alpha in tqdm(alphas):
    i+=1
    actions, rewards, Qs = generate_data(true_alpha, true_beta, n)
    
    with pm.Model() as m:
        
        alpha = pm.Beta('alpha', 1, 1, testval=0.3)
        beta = pm.HalfNormal('beta', 10, testval=5)
        
        # pm.Potential me permite definir "distribuciones" arbitrarias
        
        like = pm.Potential('like', theano_llik_td(alpha, beta, actions, rewards))
        
        step = pm.Metropolis()
        trace = pm.sample(20000, step=step)
        burned_trace=trace[1000:]
        
    par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
    data[:,:,i] = par_fit
    print(data[:,:,i])
#%%
figsize(10, 8)

#histogram of alphas

ax = plt.subplot(111)

plt.xlim(0, 1)
plt.plot(alphas, data[0,0,:],'r.', alpha=0.85)
plt.plot(alphas, alphas, 'k--', alpha=0.3)
# plt.legend(loc="upper right")
plt.xlabel(r'$\alpha$ real')
plt.ylabel(r'$\alpha$ fit')
plt.title(r"$\alpha$'s fiteados vs reales. $N=%i$"%n)
#%%
trials = np.arange(1,n+1,10)

n = 100

Rep = len(trials)

data = np.zeros((2,2,Rep))

i=-1
for h_trial in tqdm(trials):
    i+=1
    actions, rewards, Qs = generate_data(true_alpha, true_beta, n, p_r=p_r)
    
    with pm.Model() as m:
        # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
        # beta = pm.Exponential('beta', 10, testval=5)
        
        # funciona mejor con estos priors que con los anteriores
        
        alpha = pm.Beta('alpha', 1, 1, testval=0.3)
        beta = pm.HalfNormal('beta', 10, testval=5)
        
        # pm.Potential me permite definir "distribuciones" arbitrarias
        
        like = pm.Potential('like', theano_llik_td(alpha, beta, actions[:h_trial], rewards[:h_trial]))
    
        # trace = pm.sample() # sampleo por defecto
        # burned_trace=trace[1000:]
        
        # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                            # era para arreglar un error
        
        step = pm.Metropolis()
        trace = pm.sample(20000, step=step)
        burned_trace=trace[1000:]
        
    par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
    data[:,:,i] = par_fit
    print(data[:,:,i])
#%%
figsize(10, 8)

#histogram of alphas

ax = plt.subplot(111)

plt.xlim(0, n+1)
plt.plot(trials, data[0,0,:],'r.', alpha=0.85)
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
# plt.legend(loc="upper right")
plt.xlabel(r'$N$')
plt.ylabel(r'$\alpha$ fit')
plt.title(r"$\alpha$'s fiteados vs trials.")
#%%
n = 1000

trials = np.arange(1,n+1,10)

Rep = len(trials)

total = 1 #cambiar el tqdm de for

dataprom = np.zeros((2,2,Rep,total))

for k in tqdm(range(total)):
    data = np.zeros((2,2,Rep))
    i=-1
    for h_trial in tqdm(trials):
        i+=1
        actions, rewards, Qs = generate_data(true_alpha, true_beta, n, p_r=p_r)
        
        with pm.Model() as m:
            # alpha = pm.Uniform('alpha', 0, 1, testval=0.3)
            # beta = pm.Exponential('beta', 10, testval=5)
            
            # funciona mejor con estos priors que con los anteriores
            
            alpha = pm.Beta('alpha', 1, 1, testval=0.3)
            beta = pm.HalfNormal('beta', 10, testval=5)
            
            # pm.Potential me permite definir "distribuciones" arbitrarias
            
            like = pm.Potential('like', theano_llik_td(alpha, beta, actions[:h_trial], rewards[:h_trial]))
        
            # trace = pm.sample() # sampleo por defecto
            # burned_trace=trace[1000:]
            
            # idata = az.from_pymc3(trace, log_likelihood=True) # no le des bola a esto,
                                                                # era para arreglar un error
            
            step = pm.Metropolis()
            trace = pm.sample(20000, step=step)
            burned_trace=trace[1000:]
            
        par_fit = pm.summary(burned_trace).iloc[:,:2].to_numpy()
        data[:,:,i] = par_fit
        print(data[:,:,i])
    dataprom[:,:,:,k] = data
    
data = dataprom.mean(axis=3)
np.savez('RL_RL_N1000_sinrep.npz', data)
#%%
figsize(10, 8)

#histogram of alphas

ax = plt.subplot(111)

plt.xlim(0, n+1)
plt.plot(trials, data[0,0,:],'r.', alpha=0.85, label='ajuste')
plt.hlines(true_alpha, trials[0], n+1, ls='dashed', label=r'true $\alpha$')
# plt.plot(alphas, alphas, 'k--', alpha=0.3)
plt.legend(loc="upper right")
plt.xlabel(r'$N$')
plt.ylabel(r'$\alpha$ fit')
plt.title(r"$\alpha$'s fiteados vs trials. $N=%i$ (sin rep)"%n)
plt.savefig('RL_RL_sinrep_N1000.png', dpi=400)