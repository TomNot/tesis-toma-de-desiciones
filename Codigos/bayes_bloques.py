#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 10 16:07:37 2021

@author: Tomas
"""


#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb dim 12:58:13 2021

@author: Tomas
"""


from IPython.core.pylabtools import figsize
import pymc3 as pm
import numpy as np
import theano.tensor as tt
import matplotlib.pyplot as plt
import scipy.stats as stats
import pandas as pd
from tqdm import tqdm, tnrange
import seaborn as sns
import theano
import arviz as az
import copy 
import arviz as az
from scipy.integrate import quad

dim = 10

a = np.linspace(0.05, 0.95, dim)
# a = (A+B)/2

d = np.linspace(0.05, 0.95, dim)
# d = |A-B|
# supongamos que A > B entonces d = A-B

a,d = np.meshgrid(a,d)

A = a + d/2

B = a - d/2

print(A)
print(B)

#%%
Abool = np.zeros((dim,dim))
for i, row in enumerate(A):
    for j, column in enumerate(row):
        Abool[i,j] = 0<column<1
print(Abool)

Bbool = np.zeros((dim,dim))
for i, row in enumerate(B):
    for j, column in enumerate(row):
        Bbool[i,j] = 0<column<1
print(Bbool)                

aux = np.abs(Abool - Bbool)
aux = np.abs(aux - 1)

Aok = np.zeros((dim,dim))
Bok = np.zeros((dim,dim))

for i, row in enumerate(aux):
    for j, column in enumerate(row):
        Aok[i,j]=aux[i,j]*A[i,j]
        Bok[i,j]=aux[i,j]*B[i,j]

print(Aok)
print(Bok)
#%%
bloques = []
for i, row in enumerate(aux):
    for j, column in enumerate(row):
        if Aok[i,j]!=0:
            bloques.append([Aok[i,j], Bok[i,j]])
print(bloques)
#%%

def generate_data_bayes(priorA, priorB, n=100, p_r=[.4, .6]):
    # paso de b,w a e,f
    expA = np.array([priorA[0]*priorA[1], priorA[1]-priorA[0]*priorA[1]]) #[e, f]
    expB = np.array([priorB[0]*priorB[1], priorB[1]-priorB[0]*priorB[1]]) #[e, f]
    
    #experiencia verdadera (sin prior)
    expAr = np.array([0, 0]) #[e, f]
    expBr = np.array([0, 0]) #[e, f]
    
    actions = np.zeros(n, dtype=np.int)
    rewards = np.zeros(n, dtype=np.int)
    
    Ps = np.zeros((n, 2))
    
    for i in range(n):
        # print(i)
        Ps[i] = np.array([(expA[0]+1)/(np.sum(expA)+2), (expB[0]+1)/(np.sum(expB)+2)])
        # Simulate choice and reward
        
        #eleccion de maquina 0 o 1
        if stats.beta.rvs(*expA,1)>stats.beta.rvs(*expB,1):
            eleccion=0
        else:
            eleccion=1    
        resultado = np.random.rand() < p_r[eleccion]
        
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            expA[0]=expA[0]+1
            expAr[0]=expAr[0]+1
        elif eleccion==0 and resultado==0:
            expA[1]=expA[1]+1
            expAr[1]=expAr[1]+1
        elif eleccion==1 and resultado==1:
            expB[0]=expB[0]+1
            expBr[0]=expBr[0]+1
        elif eleccion==1 and resultado==0:
            expB[1]=expB[1]+1
            expBr[1]=expBr[1]+1


        # Store values
        actions[i] = eleccion
        rewards[i] = resultado

    return actions, rewards, Ps, expA, expB, expAr, expBr

def integrandB(x, eA, fA, eB, fB):
    return stats.beta.pdf(x, eA+1, fA+1)*(1-stats.beta.cdf(x, eB+1, fB+1))

def confidence(parA, parB):
    # print(parA)
    # print(parB)
    
    IA = quad(integrandB, 0, 1, args=(*parB, *parA))[0]
    IB = quad(integrandB, 0, 1, args=(*parA, *parB))[0]
    
    # print(IA, IB)
    
    N = np.sum([IA, IB])
    
    res = [IA/N, IB/N]
    
    return np.array(res)

def persistencias(actions, rewards):
    n = len(actions)
    pers = 0
    for t in range(n):
        if rewards[t]==0:
            if t<(n-1) and actions[t+1]==actions[t]:
                pers+=1
    return pers

agentes = 1000
# defino los parámetros de los agentes (recorro toda la grilla)
bs = np.linspace(0.05,0.95,agentes)
w = 8

# defino la cantidad de trials
n = 100

# inicializo arrays para guardar los resultados
# resultados = np.zeros((n,2,len(bloques),len(bs)))
# Qs = np.zeros((n,2,len(bloques),len(bs)))
# confs = np.zeros((2,len(bloques),len(bs)))
cok = np.zeros((len(bloques),len(bs)))
cint = np.zeros((len(bloques),len(bs)))
pok = np.zeros((len(bloques),len(bs)))
pnb = np.zeros((len(bloques),len(bs)))
ex = np.zeros((len(bloques),len(bs)))
pers = np.zeros((len(bloques),len(bs)))

for i,p in tqdm(enumerate(bs), desc='Loop 0'):    
    # print(a_s,a_f)
    for b,p_r in enumerate(bloques):
        # print('\n'+'p\'s={}'.format(p_r)+'\n')
        prior = [p,w]
        actions, rewards, Ps, expA, expB, expAr, expBr = generate_data_bayes(prior, prior, n=n, p_r=p_r)
        
        cok[b,i] = np.diff(Ps[-1])
        Psnb = np.array([(expAr[0]+1)/(np.sum(expAr)+2), (expBr[0]+1)/(np.sum(expBr)+2)])
        # print(Psnb)
        pok[b,i] = np.diff(Psnb)
        s0_real, f0_real = expA
        s1_real, f1_real = expB
        
        s0, f0 = expAr
        s1, f1 = expBr
        
        #computo las probabilidades
        prob0, prob1 = confidence([s0_real, f0_real], [s1_real,f1_real])
        prob0nb, prob1nb = confidence([s0, f0], [s1,f1])
        
        # # print('prob0='+str(prob0))
        # # print('prob1='+str(prob1))
        
        eleccion = actions[-1]
        
        # #probabilidad de la eleccion dado el prior 
        if eleccion==0:
            cint[b,i] = prob0
            pnb[b,i] = prob0nb
        elif eleccion==1:
            cint[b,i] = prob1
            pnb[b,i] = prob1nb
        ex[b,i] = np.sum(np.abs(np.diff(actions)))
        pers[b,i] = persistencias(actions,rewards)
        
#%%
bloques = np.array(bloques)
A = bloques[:,0]
B = bloques[:,1]

a = (A+B)/2
d = A-B
dif = 1 - d

#%%
# import statsmodels.api as sm           ## Este proporciona funciones para la estimación de muchos modelos estadísticos
import statsmodels.formula.api as smf  ## Permite ajustar modelos estadísticos utilizando fórmulas de estilo R

def sigmoidea(coefs):
    return 1/(1+np.exp(-coefs[0]/-coefs[1]))

sigmas = np.zeros((len(bs)))
sigmas_int = np.zeros((len(bs)))
sigmasnb = np.zeros((len(bs)))
sigmas_intnb = np.zeros((len(bs)))

agentes=10000

cok = np.abs(cok)

R2s = np.zeros((len(bs)))
R2sint = np.zeros((len(bs)))

for i in tqdm(range(len(bs))):
    
    dtint = pd.DataFrame(data=np.matrix([a,dif,cint[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    dt = pd.DataFrame(data=np.matrix([a,dif,cok[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    
    dtintnb = pd.DataFrame(data=np.matrix([a,dif,pnb[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    dtnb = pd.DataFrame(data=np.matrix([a,dif,pok[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    
    mod = smf.ols('confianza ~ atractivo + dificultad', data=dt).fit()
    modint = smf.ols('confianza ~ atractivo + dificultad', data=dtint).fit()
    modnb = smf.ols('confianza ~ atractivo + dificultad', data=dtnb).fit()
    modintnb = smf.ols('confianza ~ atractivo + dificultad', data=dtintnb).fit()
    # print(mod.summary())
    R2s[i] = mod.rsquared
    R2sint[i] = modint.rsquared
    coefs = mod.params
    coefsint = modint.params
    coefsnb = modnb.params
    coefsintnb = modintnb.params
    sigma = sigmoidea(coefs[1:])
    sigmaint = sigmoidea(coefsint[1:])
    sigmanb = sigmoidea(coefsnb[1:])
    sigmaintnb = sigmoidea(coefsintnb[1:])
    print(modint.rsquared)
    # print(coefs)
    # print(sigma)
    # print(coefsint)
    # print(sigmaint)
    
    sigmas[i] = sigma
    sigmas_int[i] = sigmaint
    sigmasnb[i] = sigmanb
    sigmas_intnb[i] = sigmaintnb
np.savez('dificultad_comportamiento_bayes_N%i_%iagentes_%ibloques_con_pers.npz'%(n, agentes, len(bloques)), ex, cok, cint, pers, pok, pnb, bs, sigmas, sigmas_int, sigmasnb, sigmas_intnb, R2s, R2sint, bloques)
#%%
arrays = np.load('dificultad_comportamiento_bayes_N100_10000agentes_50bloques_con_pers.npz')

ex, cok, cint, pers, pok, pnb, bs, sigmas, sigmas_int, sigmasnb, sigmas_intnb, bloques = [arrays['arr_%i'%i] for i in range(12)] #, pers, pok
#%%
dom = ex.mean(axis=0)
df = pd.DataFrame(data=np.matrix([dom,sigmas]).T, columns=['alternancias','sigma'])
dfint = pd.DataFrame(data=np.matrix([dom,sigmas_int]).T, columns=['alternancias','sigma'])
mod1 = smf.ols('sigma ~ alternancias', data=df).fit()
mod1int = smf.ols('sigma ~ alternancias', data=dfint).fit()
# print(mod.summary())
coefs = mod1.params
coefsint = mod1int.params
#%%
def lineal(x,m,b):
    return m*x+b
domnorm = dom/np.max(dom)
coefsnb = np.polyfit(domnorm, sigmasnb, 1)

plt.figure()
plt.plot(domnorm, sigmasnb, '.')
plt.plot(domnorm, lineal(domnorm, *coefsnb), 'r-', alpha=0.6)
plt.hlines(0.5, min(domnorm), max(domnorm), ls='dashed', label=r'$0.5$', zorder=10)
plt.legend(loc="upper right")
plt.xlabel(r'Exploración')
plt.ylabel(r'$\sigma_{NB}$')
plt.savefig('bayes_nb_%ibloques_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)

coefs = np.polyfit(domnorm, sigmas, 1)

plt.figure()
plt.plot(domnorm, sigmas_int, '.')
plt.plot(domnorm, lineal(domnorm, *coefs), 'r-', alpha=0.6)
plt.hlines(0.5, min(domnorm), max(domnorm), ls='dashed', label=r'$0.5$', zorder=10)
plt.legend(loc="upper right")
plt.xlabel(r'Exploración')
plt.ylabel(r'$\sigma$')
plt.savefig('bayes_%ibloques_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
#%%
from scipy.stats import pearsonr
corr, p = pearsonr(domnorm, sigmasnb)
corr, p = pearsonr(domnorm, sigmas_int)
#%%

coefs_intnb = np.polyfit(dom, sigmasnb, 1)
plt.figure()

plt.plot(dom, sigmas_intnb, '.')
plt.plot(dom, lineal(dom, *coefs_intnb), 'r-', alpha=0.6)
plt.hlines(0.5, min(dom), max(dom), ls='dashed', label=r'$0.5$', zorder=10)
plt.legend(loc="upper right")
plt.xlabel(r'Alternancias promedio')
plt.ylabel(r'$\sigma_{\int}$')
# plt.savefig('int_bayes_%ibloques_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
#%%


h = plt.hist2d(domnorm, sigmas, bins=(50, 50), cmap=plt.cm.jet)#, vmin=0, vmax=20)

cbar = plt.colorbar(h[3])
# cbar.set_label('Confianza', rotation=0)
cbar.ax.set_title('Concentración')
# plt.hexbin(dom,sigmas_int, vmin=0, vmax=8)
# plt.xlabel(r'$\Delta \alpha$')
# plt.ylabel(r'$\sigma_{\int}$')
plt.hlines(0.5, min(domnorm), max(domnorm), color='w', alpha=0.95, ls='dashed')
plt.xlabel(r'$Exploración$')
plt.ylabel(r'$\sigma$')
plt.ylim(0.4, 1)
# plt.xlim(4,30)
# plt.savefig('color_alt_sigmasnb_bayes_%ibloques_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
# %%
# #%%
# plt.figure()
# # plt.plot(bs, sigmas_int, '.')
# # plt.plot(bs, lineal(bs, *coefsint), 'r-', alpha=0.6)
# h=plt.hist2d(bs, sigmas_int, bins=(50, 50), cmap=plt.cm.jet)#, vmin=0, vmax=20)
# cbar = plt.colorbar(h[3])
# # cbar.set_label('Confianza', rotation=0)
# cbar.ax.set_title('Concentración')
# plt.hlines(0.5, bs[0], bs[-1], ls='dashed', color='w', label=r'$0.5$')
# # plt.legend(loc="upper right")
# plt.xlabel(r'$b$')
# plt.ylabel(r'$\sigma$')
# plt.ylim(0.4,1)
# plt.savefig('color_bs_sigmas_bayes_%ibloques_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)


# #%%
# df = pd.DataFrame(data=np.matrix([bs,sigmas]).T, columns=['alternancias','sigma'])
# dfint = pd.DataFrame(data=np.matrix([bs,sigmas_int]).T, columns=['alternancias','sigma'])
# mod1 = smf.ols('sigma ~ alternancias', data=df).fit()
# mod1int = smf.ols('sigma ~ alternancias', data=dfint).fit()
# # print(mod.summary())
# coefs = mod1.params
# coefsint = mod1int.params
# #%%
# plt.figure()
# plt.plot(bs, sigmas, '.')
# plt.plot(bs, lineal(bs, *coefs), 'r-', alpha=0.6)
# plt.hlines(0.5, bs[0], bs[-1], ls='dashed', label=r'$0.5$')
# plt.legend(loc="upper right")
# plt.xlabel(r'$b$')
# plt.ylabel(r'$\sigma$')
# # plt.savefig('bayes_%ibloques_prior_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
# #%%
# plt.figure()
# plt.plot(bs, sigmas_int, '.')
# plt.plot(bs, lineal(bs, *coefsint), 'r-', alpha=0.6)
# plt.hlines(0.5, bs[0], bs[-1], ls='dashed', label=r'$0.5$')
# plt.legend(loc="upper right")
# plt.xlabel(r'$b$')
# plt.ylabel(r'$\sigma_{\int}$')
# # plt.savefig('int_bayes_%ibloques_prior_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
# #%%
# model = np.polyfit(bs,dom, 1)

# plt.figure()
# plt.plot(bs,dom, '.', ms=0.5)
# plt.plot(bs, lineal(bs,model[1],model[0]), '-', color='k', alpha=0.9)
# # plt.hlines(0.5, min(dom), max(dom), ls='dashed', label=r'$0.5$')
# # plt.legend(loc="upper right")
# plt.ylabel(r'Exploración')
# plt.xlabel(r'$b$')
# plt.xlim(0, 1)
# plt.grid()
# plt.savefig('bayes_%ibloques_bs_vs_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)

# corr, p = pearsonr(dom, bs)
# #%%
# persprom = pers.mean(axis=0)
# persnorm = persprom/np.max(persprom)

# plt.figure()
# h=plt.hist2d(persnorm, sigmas_int, bins=(50, 50), cmap=plt.cm.jet)#, vmin=0, vmax=20)
# cbar = plt.colorbar(h[3])
# # cbar.set_label('Confianza', rotation=0)
# cbar.ax.set_title('Concentración')
# # plt.plot(persprom, sigmas_int, '.')
# # plt.plot(pers[0,:], lineal(pers[0,:], *coefsint), 'r-', alpha=0.6)
# plt.hlines(0.5, min(persnorm), max(persnorm), color='w', ls='dashed', label=r'$0.5$')
# # plt.legend(loc="upper right")
# plt.xlabel(r'$Explotación$')
# plt.ylabel(r'$\sigma$')
# plt.ylim(0.4, 1)
# plt.show()
# plt.savefig('color_pers_sigmas_bayes_%ibloques_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
# #%%
# from scipy.stats import pearsonr 

# c = np.sort(cint[0,:])
# dif = cok[0,:][np.argsort(cint[0,:])]

# mitadc = int(len(c)/2)
# c1 = c[10:mitadc]
# dif1 = dif[10:mitadc]

# corr, p = pearsonr(c1, dif1)

# bloque = 0

# corrok, pok = pearsonr(dom, sigmas)

# corrok, pok = pearsonr(dom, sigmas_int)

# corrint, pint = pearsonr(cint[bloque,:], sigmas_int)

# corrpers, ppers = pearsonr(persprom, sigmas_int)
# #%%

# arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-desiciones/Datos_simulados/dificultad_comportamiento_bayes_N100_2agentes_5488bloques_con_pers')

# ex, cok, cint, pers, pok, pnb, bs, sigmas, sigmas_int, bloques = [arrays['arr_%i'%i] for i in range(10)]

# agente = 9
# atr = a.flatten()
# dificultad = dif.flatten()

# # ax = plt.axes(projection='3d')
# plt.scatter(x=atr, y=dificultad, c=cint[:,agente], cmap='viridis')#, vmin=0, vmax=1, linewidth=0.5)

# # plt.plot(persprom, sigmas_int, '.')
# # plt.plot(pers[0,:], lineal(pers[0,:], *coefsint), 'r-', alpha=0.6)
# # plt.hlines(0.5, min(persnorm), max(persnorm), color='w', ls='dashed', label=r'$0.5$')
# # plt.legend(loc="upper right")
# plt.xlabel(r'Dificultad')
# plt.ylabel(r'Atractivo')
# # plt.clabel(r'Confianza')
# plt.colorbar()
# plt.show()
# #%%
# from matplotlib import cm
# from matplotlib.ticker import LinearLocator
# from mpl_toolkits.mplot3d import Axes3D
# from scipy.interpolate import griddata

# fig, ax = plt.subplots(subplot_kw={"projection": "3d"})

# # Make data.
# x = atr
# y = dificultad

# # construcción de la grilla 2D
# xi = np.linspace(min(x), max(x))
# yi = np.linspace(min(y), max(y))
# X, Y = np.meshgrid(xi, yi)
# z = cint[:,agente]
 

# # interpolación
# #Z = griddata(x, y, z, xi, yi, interp='linear')
# Z = griddata((x, y), z, (xi[None,:], yi[:,None]), method='cubic')

# ax.plot_surface(X, Y, Z, rstride=1, cstride=1, cmap=cm.jet,linewidth=1, antialiased=True)


# # Plot the surface.
# # surf = ax.plot_surface(X, Y, Z, cmap=cm.coolwarm,
#                        # linewidth=0, antialiased=False)
