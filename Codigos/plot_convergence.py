#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar  2 17:41:25 2021

@author: Tomas
"""

import numpy as np
import matplotlib.pyplot as plt

arrays = np.load('bayes_RLas_N1000_1_esc(1 rep)_.2, .6_w50b0.6.npz')

p_r = [0.2, 0.6]

n = 1000

total = 1

dataprom = arrays['arr_0']
resultados = arrays['arr_1']

parfit = dataprom.mean(axis=3)[:,0,:]
errpar = dataprom.mean(axis=3)[:,1,:]/np.sqrt(total)

trials = np.arange(1,n+1,50)

hasta = 20

ylabels = [r'$\beta$', r'$\alpha_-$', r'$\alpha_+$']
names = ['beta', 'alfa_mas', 'alfa_menos']

alfa_menos = parfit[1][4:].mean()
err_alfa_menos = errpar[1][4:].std()
alfa_mas = parfit[2][4:].mean()
err_alfa_mas = errpar[2][4:].std()

for i in range(len(parfit)):
        
    plt.figure()
    
    plt.plot(trials, parfit[i],'.', color = 'C%i'%1, alpha=0.85, ms=5, label=r"fit")
    plt.errorbar(trials, parfit[i], errpar[i], fmt='none', ecolor = 'C%i'%1, alpha=0.5)
    # plt.plot(alphas, alphas, 'k--', alpha=0.3)
    #plt.xticks(ticks=dom, labels=p_r_list)
    plt.hlines(0, trials[0], trials[-1], ls='dashed',label=r'0')
    if i==1:
        plt.hlines(alfa_menos, trials[0], trials[-1], ls='dashed', color='k',alpha=0.2,label=r'%.2f'%alfa_menos)
    if i==2:
        plt.hlines(alfa_mas, trials[0], trials[-1], ls='dashed', color='k',alpha=0.2,label=r'%.2f'%alfa_mas)
    if i!=0:
        plt.ylim(0,1)
    plt.legend(loc="upper right")
    plt.xlabel(r'trials $N$')
    plt.ylabel(ylabels[i])
    # plt.ylabel(r'$\alpha$')
    plt.title(r"$b_{true} = 0.6$. $N=%i$ (%i rep)"%(n, total))
    plt.grid()

    plt.savefig(names[i]+'_bayes_RLas_N500_1_esc(1 rep)_pA%.1f_pB%.1f_b0.6.png'%(p_r[0],p_r[1]), dpi=400)
#%%
# plt.figure()

# plt.plot(trials, dataprom[1,0,:],'.', color = 'C%i'%1, alpha=0.85, ms=2, label=r"$\beta$ fit")
# plt.errorbar(trials, dataprom[1,0,:], dataprom[1,1,:], fmt='none', ecolor = 'C%i'%1, alpha=0.1)
# # plt.plot(alphas, alphas, 'k--', alpha=0.3)
# #plt.xticks(ticks=dom, labels=p_r_list)
# #plt.ylim(0,1)
# plt.legend(loc="upper right")
# plt.xlabel(r'trials $N$')
# plt.ylabel(r'$\beta$')
# plt.title(r"$\beta$'s vs trials. $b_{true} = 0.6$. $N=%i$ (%i rep)"%(n, total))
# plt.grid()

# plt.savefig('beta_bayes_RL_N500_1_esc(1 rep)_pA01_pB02.png', dpi=400)
#%%
from scipy.optimize import curve_fit

def expo(x,a,b,c):
    return a*np.exp(-b*x)+c

x = trials
y = dataprom[0,0,:hasta,0,0]
yerr = dataprom[0,1,:hasta,0,0]

# pruebo
p0 = [0.4,1/80, 0.08]

popt, pcov = curve_fit(expo, x, y, sigma=yerr, p0=p0)

y_fit = expo(x, *popt)

plt.figure()


plt.plot(x, y,'.', color = 'C%i'%1, alpha=0.95, ms=5, label=r"$\alpha$ fit")
plt.errorbar(x, y, yerr, fmt='none', ecolor = 'C%i'%1, alpha=0.5)
# plt.plot(x, expo(x, *popt), 'k--', alpha=0.3)
#plt.xticks(ticks=dom, labels=p_r_list)
plt.hlines(0, trials[0], trials[-1], ls='dashed',label=r'0')
# plt.ylim(0,1)
# plt.legend(loc="upper right")
plt.xlabel(r'trials $N$')
plt.ylabel(r'$\alpha$')
plt.title(r"$\alpha$'s vs trials. $b_{true} = 0.6$. $N=%i$ (%i rep)"%(n, total))
plt.grid()
plt.savefig('alpha_bayes_RLas_N500_1_esc(1 rep)_pA%.1f_pB%.1f_.png'%(p_r[0],p_r[1]), dpi=400)
#%%
arrays = np.load('RL_RL_N1000_1_esc(1 rep).npz')

p_r = [0.6, 0.8]

n = 1000

total = 1

dataprom = arrays['arr_0']
resultados = arrays['arr_1']

trials = np.arange(1,n+1,50)

hasta = 20

alfa = 0.5

from scipy.optimize import curve_fit

def expo(x,a,b,c):
    return a*np.exp(-b*x)+c

x = trials
y = dataprom[0,0,:hasta,0]
yerr = dataprom[0,1,:hasta,0]

# pruebo
p0 = [0.4,1/80, 0.08]

popt, pcov = curve_fit(expo, x, y, sigma=yerr, p0=p0)

y_fit = expo(x, *popt)

plt.figure()


plt.plot(x, y,'.', color = 'C%i'%1, alpha=0.95, ms=5, label=r"$\alpha$ fit")
plt.errorbar(x, y, yerr, fmt='none', ecolor = 'C%i'%1, alpha=0.5)
# plt.plot(x, expo(x, *popt), 'k--', alpha=0.3)
#plt.xticks(ticks=dom, labels=p_r_list)
plt.hlines(alfa, trials[0], trials[-1], ls='dashed',label=r'$\alpha$ true')
# plt.ylim(0,1)
# plt.legend(loc="upper right")
plt.xlabel(r'trials $N$')
plt.ylabel(r'$\alpha$')
plt.title(r"$\alpha$'s vs trials. $\alpha_{true} = 0.5$. $N=%i$ (%i rep)"%(n, total))
plt.grid()
plt.savefig('alpha_RL_RL_N1000_1_esc(1 rep)_pA%.1f_pB%.1f.png'%(p_r[0],p_r[1]), dpi=400)
#%%
arrays = np.load('RLas_RLas_N1000_1_esc(1 rep)_.2, .6_alfas.7.4.npz')

# p_r = [0.2, 0.8]

# n = 1000

# total = 1

true_alpha_s = .4
true_alpha_f = .7
true_beta = 5
    

dataprom = arrays['arr_0']
resultados = arrays['arr_1']

parfit = dataprom.mean(axis=3)[:,0,:20]
errpar = dataprom.mean(axis=3)[:,1,:20]/np.sqrt(total)

trials = np.arange(1,n+1,50)

hasta = 20

ylabels = [r'$\beta$', r'$\alpha_-$', r'$\alpha_+$']
names = ['beta', 'alfa_mas', 'alfa_menos']

desde = 9

alfa_menos = parfit[1][desde:].mean()
err_alfa_menos = errpar[1][desde:].std()
alfa_mas = parfit[2][desde:].mean()
err_alfa_mas = errpar[2][desde:].std()

for i in range(len(parfit)):
        
    plt.figure()
    
    plt.plot(trials, parfit[i],'.', color = 'C%i'%1, alpha=0.85, ms=5, label=r"fit")
    plt.errorbar(trials, parfit[i], errpar[i], fmt='none', ecolor = 'C%i'%1, alpha=0.5)
    # plt.plot(alphas, alphas, 'k--', alpha=0.3)
    #plt.xticks(ticks=dom, labels=p_r_list)
    # plt.hlines(0, trials[0], trials[-1], ls='dashed',label=r'0')
    if i==1:
        plt.hlines(alfa_menos, trials[0], trials[-1], ls='dashed', color='k',alpha=0.2,label=r'%.2f'%alfa_menos)
        plt.hlines(true_alpha_f, trials[0], trials[-1], ls='dashed', color='k',alpha=1,label=r'$\alpha_{-}^{true}$ = %.2f'%true_alpha_f)
    if i==2:
        plt.hlines(alfa_mas, trials[0], trials[-1], ls='dashed', color='k',alpha=0.2,label=r'%.2f'%alfa_mas)
        plt.hlines(true_alpha_s, trials[0], trials[-1], ls='dashed', color='k',alpha=1,label=r'$\alpha_{+}^{true}$ = %.2f'%true_alpha_s)
    if i!=0:
        plt.ylim(0,1)
    plt.legend(loc="upper right")
    plt.xlabel(r'trials $N$')
    plt.ylabel(ylabels[i])
    # plt.ylabel(r'$\alpha$')
    # if i==1:
    #     plt.title(r"$\alpha_-^{true} = 0.3$. $N=%i$ (%i rep)"%(n, total))
    # if i==2:
    #     plt.title(r"$\alpha_+^{true} = 0.6$. $N=%i$ (%i rep)"%(n, total))
    plt.grid()

    plt.savefig(names[i]+'_RLas_RLas_N1000_1_esc(1 rep)_pA%.1f_pB%.1f_alfas.7.4.png'%(p_r[0],p_r[1]), dpi=400)