#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 11 17:36:11 2021

@author: Tomas
"""



from IPython.core.pylabtools import figsize
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import statsmodels.api as sm           ## Este proporciona funciones para la estimación de muchos modelos estadísticos
import statsmodels.formula.api as smf  ## Permite ajustar modelos estadísticos utilizando fórmulas de estilo R
from tqdm import tqdm

arrays = np.load('/home/Tomas/Escritorio/Bandit/pyMC3/tesis-toma-de-desiciones/Datos_simulados/dificultad_comportamiento_bayes_N100_1000agentes_50bloques.npz')

ex, cok, cint, pok, pnb, bs, sigmas, sigmas_int, bloques = [arrays['arr_%i'%i] for i in range(9)]

w = 8
n = 100
agentes = 100
#%%
A = bloques[:,0]
B = bloques[:,1]

a = (A+B)/2
d = A-B
dif = 1 - d
#%%
def sigmoidea(coefs):
    return 1/(1+np.exp(-coefs[0]/-coefs[1]))

sigmas = np.zeros((len(bs)))
sigmas_int = np.zeros((len(bs)))

for i in tqdm(range(len(bs))):
    
    dtint = pd.DataFrame(data=np.matrix([a,dif,pnb[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    dt = pd.DataFrame(data=np.matrix([a,dif,pok[:,i]]).T, columns=['atractivo','dificultad','confianza'])
    
    mod = smf.ols('confianza ~ atractivo + dificultad', data=dt).fit()
    modint = smf.ols('confianza ~ atractivo + dificultad', data=dtint).fit()
    # print(mod.summary())
    coefs = mod.params
    coefsint = modint.params
    # print(coefs)
    sigma = sigmoidea(coefs[1:])
    sigmaint = sigmoidea(coefsint[1:])
    sigmas[i] = sigma
    sigmas_int[i] = sigmaint
#%%

dom = ex.mean(axis=0)

df = pd.DataFrame(data=np.matrix([dom,sigmas]).T, columns=['alternancias','sigma'])
dfint = pd.DataFrame(data=np.matrix([dom,sigmas_int]).T, columns=['alternancias','sigma'])
mod1 = smf.ols('sigma ~ alternancias', data=df).fit()
mod1int = smf.ols('sigma ~ alternancias', data=dfint).fit()
# print(mod.summary())
coefs = mod1.params
coefsint = mod1int.params
#%%
def lineal(x,b,m):
    return m*x+b

plt.figure()
plt.plot(dom, sigmas, '.')
plt.plot(dom, lineal(dom, *coefs), 'r-', alpha=0.6)
plt.hlines(0.5, min(dom), max(dom), ls='dashed', label=r'$0.5$')
plt.legend(loc="upper right")
plt.xlabel(r'Alternancias promedio')
plt.ylabel(r'$\sigma$')
plt.savefig('bayes_%ibloques_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
#%%
plt.figure()
dom = ex.mean(axis=0)
plt.plot(dom, sigmas_int, '.')
plt.plot(dom, lineal(dom, *coefsint), 'r-', alpha=0.6)
plt.hlines(0.5, min(dom), max(dom), ls='dashed', label=r'$0.5$')
plt.legend(loc="upper right")
plt.xlabel(r'Alternancias promedio')
plt.ylabel(r'$\sigma_{\int}$')
plt.savefig('int_bayes_%ibloques_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
#%%
df = pd.DataFrame(data=np.matrix([bs,sigmas]).T, columns=['alternancias','sigma'])
dfint = pd.DataFrame(data=np.matrix([bs,sigmas_int]).T, columns=['alternancias','sigma'])
mod1 = smf.ols('sigma ~ alternancias', data=df).fit()
mod1int = smf.ols('sigma ~ alternancias', data=dfint).fit()
# print(mod.summary())
coefs = mod1.params
coefsint = mod1int.params
#%%
plt.figure()
plt.plot(bs, sigmas, '.')
plt.plot(bs, lineal(bs, *coefs), 'r-', alpha=0.6)
plt.hlines(0.5, bs[0], bs[-1], ls='dashed', label=r'$0.5$')
plt.legend(loc="upper right")
plt.xlabel(r'$b$')
plt.ylabel(r'$\sigma$')
plt.savefig('bayes_%ibloques_prior_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
#%%
plt.figure()
plt.plot(bs, sigmas_int, '.')
plt.plot(bs, lineal(bs, *coefsint), 'r-', alpha=0.6)
plt.hlines(0.5, bs[0], bs[-1], ls='dashed', label=r'$0.5$')
plt.legend(loc="upper right")
plt.xlabel(r'$b$')
plt.ylabel(r'$\sigma_{\int}$')
plt.savefig('int_bayes_%ibloques_prior_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)
#%%
plt.figure()
plt.plot(dom, bs, '.')
# plt.hlines(0.5, min(dom), max(dom), ls='dashed', label=r'$0.5$')
# plt.legend(loc="upper right")
plt.xlabel(r'Alternancias promedio')
plt.ylabel(r'$b$')
plt.savefig('bayes_%ibloques_bs_vs_alt_prom_w%i_n%i_%iagentes.png'%(len(bloques), w, n, agentes), dpi=400)

#%% 
plt.hexbin(dom,sigmas_int)