#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Sep 14 11:29:23 2020

@author: Tomas
"""
import numpy as np
from scipy.stats import beta
from scipy.stats import binom
from scipy.stats import norm
import seaborn as sns
import matplotlib.pyplot as plt
from tqdm import tqdm

## Bayesian inference over parameters Thompson sampling
#largo del bloque bandit
num_trials=100  

#probabilidades de pago verdaderas
p0=0.2
p1=0.8

#prior del agente thompson sampling
b0, b1 = 0.4, 0.4
w0, w1 = 5, 5

#x es el rango de las betas
x=np.arange(start=1/50,stop=49/50,step=1/50) 

#números de repeticiones de la simulación
total=100

dataprom = np.zeros((2, total)) # [[mean, std], Repeticiones]

for k in tqdm(range(total)):   
    
    #experiencia verdadera (sin prior)
    s0_total=0
    f0_total=0
    s1_total=0
    f1_total=0

    #inicializo las listas
    data = np.zeros((num_trials, 2))
    p_prob = np.zeros((len(x), num_trials))
    prob = np.zeros((len(x), num_trials))

    for l in tqdm(range(num_trials)):
        print(l)
        #reparametrizo para usar betarnd
        if l==0:
            s0=b0*w0
            f0=w0-b0*w0
            s1=b1*w1
            f1=w1-b1*w1
    
        #eleccion de maquina 0 o 1
        if beta.rvs(s0,f0,1)>beta.rvs(s1,f1,1):
            eleccion=0
        else:
            eleccion=1
    
        #resultado de la eleccion
        if eleccion==0:
            resultado=binom.rvs(1,p0,1)
        elif eleccion==1:
            resultado=binom.rvs(1,p1,1)
    
        #actializo parametros de Beta posterior (usada para comportamiento)
        #y actualizo la experiencia real (usada luego para el fit)
        if eleccion==0 and resultado==1:
            s0=s0+1
            s0_total=s0_total+1
        elif eleccion==0 and resultado==0:
            f0=f0+1
            f0_total=f0_total+1
        elif eleccion==1 and resultado==1:
            s1=s1+1
            s1_total=s1_total+1
        elif eleccion==1 and resultado==0:
            f1=f1+1
            f1_total=f1_total+1
    
    
        data[l,:]= [eleccion, resultado]
       
        i_0=-1
        #llamo b_fit al unico parametros libre (asumo b0=b1 y w verdadero)
        for b_fit in x:
            i_0=i_0+1
              
            s0_p = b_fit*w0
            f0_p = (w0-b_fit*w0)
            s1_p = b_fit*w1
            f1_p = (w1-b_fit*w1)
            
            # print(s0_p+s0_total,f0_p+f0_total)
            # print(s1_p+s1_total,f1_p+f1_total)
            # print(l, b_fit)
            #computo pdf's y cdf's
            pxA=beta.pdf(x,s0_p+s0_total,f0_p+f0_total)
            cdfBxA=1-beta.cdf(x,s1_p+s1_total,f1_p+f1_total)
            pprob1=np.sum(pxA*cdfBxA)/len(x)
    
            pxB=beta.pdf(x,s1_p+s1_total,f1_p+f1_total)
            cdfAxB=1-beta.cdf(x,s0_p+s0_total,f0_p+f0_total)
            pprob0=np.sum(pxB*cdfAxB)/len(x)
           
            prob0=pprob0/(pprob0+pprob1)
            prob1=pprob1/(pprob0+pprob1)
           
            #probabilidad de la eleccion dado el prior (labeled by i_0)
            if eleccion==0:
                p_prob[i_0,l]=np.log(prob0)
            elif eleccion==1:
                p_prob[i_0,l]=np.log(prob1)
           
            #i.i.d. + np.log sapce
            prob[i_0,l]=np.sum(p_prob[i_0,1:l])
               
        #normalizo y paso a linear space
        prob[:,l]=np.exp(prob[:,l])/np.sum(np.exp(prob[:,l]))
        
    plt.plot(x, prob[:,-1], label='Rep = %i'%k)
    # Plot formatting
    plt.legend(prop={'size': 6})
    plt.title(r'Density Plot $b$')
    plt.xlabel(r'$b$ value')
    plt.ylabel('Density')
      
    dataprom[:,k] = [np.average(x, weights=prob[:,-1]), np.sqrt(np.cov(x, aweights=prob[:,-1]))]
    
data = [dataprom[:,0].mean(), dataprom[:,1].mean()/np.sqrt(total)]
np.savez('bayes_bayes_N100_w5.npz', dataprom)
#%%

trials = np.arange(1,num_trials+1)

x0 = trials
y = x

X, Y = np.meshgrid(x0, y)
Z = prob

# plt.contour(X, Y, Z, 20, cmap='RdGy');
plt.contourf(X, Y, Z, 20, cmap='RdGy')
plt.colorbar();
plt.text(np.argmax(prob[:,-1])*1.1,x[np.argmax(prob[:,-1])]*1.1, '$%0.3f$'%x[np.argmax(prob[:,-1])], fontsize=20, color='b')
plt.hlines(x[np.argmax(prob[:,-1])],0,100)


plt.xlabel('Trial')
plt.ylabel('Parameter')

plt.tight_layout()
plt.show()